<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weekdays', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->unsignedBigInteger('day');
            $table->timestamps();
        });

        $weekdays = [
            ['title' => 'Sunday', 'day' => 0],
            ['title' => 'Monday', 'day' => 1],
            ['title' => 'Tuesday', 'day' => 2],
            ['title' => 'Wednesday', 'day' => 3],
            ['title' => 'Thursday', 'day' => 4],
            ['title' => 'Friday', 'day' => 5],
            ['title' => 'Saturday', 'day' => 6],
        ];

        DB::table('weekdays')->insert($weekdays);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weekdays');
    }
};
