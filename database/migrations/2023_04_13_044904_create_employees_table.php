<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->text('emp_code');
            $table->text('first_name');
            $table->text('last_name')->nullable();
            $table->text('nickname')->nullable();
            $table->date('hire_date');
            $table->text('gender');
            $table->unsignedBigInteger('emp_type');
            $table->text('mobile')->nullable();
            $table->text('email');
            $table->unsignedBigInteger('area_code')->nullable();
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('position_id');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->dateTime('sync_time');
            $table->softDeletes();
            $table->timestamps();
        });
    }


    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
