<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->string('address');
            $table->string('port');
            $table->string('username');
            $table->string('password');
            $table->boolean('default');
            $table->timestamps();
        });

        DB::table('devices')->insert([
            'name' => 'CSE',
            'address' => '172.168.250.250',
            'port' => '8081', 
            'username' => 'test',
            'password' => 'CSE123456', 
            'default' => true, 
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
};
