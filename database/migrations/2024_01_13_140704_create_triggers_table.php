<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('triggers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('punch_id')->nullable();
            $table->string('emp_code');
            $table->dateTime('punch_time');
            $table->dateTime('upload_time');
            $table->string('terminal_sn')->nullable();
            $table->string('department')->nullable();
            $table->boolean('manual_entry')->default(false);
            $table->unsignedBigInteger('manual_entry_by')->nullable();
            $table->dateTime('sync_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('triggers');
    }
};
