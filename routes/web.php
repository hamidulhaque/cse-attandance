<?php

use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DaywiseController;
use App\Http\Controllers\DeviceController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\HolidayController;
use App\Http\Controllers\LeaveController;
use App\Http\Controllers\ManualUpdateController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\WeekendController;
use App\Models\Employee;
use Illuminate\Support\Facades\Route;



Route::get('/dashboard', [DashboardController::class, 'index'])->middleware(['auth', 'verified'])->name('dashboard');


Route::get('/login', function () {
    return redirect('/');
});




Route::middleware(['auth', 'verified'])->group(function () {

    Route::prefix("/")->group(function () {
        Route::controller(DashboardController::class)->group(function () {
            Route::get('/test', 'test');
            Route::get('/test2', 'test2');
            Route::get('/device-syncronization', 'syncDevice')->name('deviceSync.view');

        });


        Route::controller(ManualUpdateController::class)->group(function () {
            Route::prefix("/manual-update")->group(function () {
                Route::get("/attandance", "attandenceView")->name("manualAttandance.view");
                Route::post("/attandance", "attandencePost")->name("manualAttandance.post");
                Route::get('/download-sample-csv', 'downloadSampleCsv')->name('manual.sample.csv');
            });
        });



        Route::post('/sync-biotime-data', function () {
            Artisan::call('sync:biotime-data');
            return response()->json(['success' => true, 'message' => 'Sync command initiated.']);
        })->name('sync:biotime-data');

        Route::controller(ReportController::class)->group(function () {

            Route::get('/employee-report/indivitual', 'individualReportView')->name('individualReport.View');
            Route::post('/employee-report/indivitual', 'individualReports')->name('individual.Reports');


        });


        Route::prefix('/employees')->group(function () {
            Route::controller(EmployeeController::class)->group(function () {
                Route::get('/', 'list')->name('employee.list');
                Route::post('/employees-dropdown-list', 'empDropDownList')->name('emp.DropList');

            });


            // Route::get('/', [EmployeeController::class, 'index'])->name('employee.list');
        });


        Route::prefix('/day-wise')->group(function () {
            Route::controller(DaywiseController::class)->group(function () {
                Route::get('/report', 'dayWise')->name('daywise.view');
                Route::post('/report', 'DayWiseReport')->name('daywise.report');

            });
        });


        Route::prefix('/holiday')->group(function () {
            Route::controller(HolidayController::class)->group(function () {
                Route::get('/', 'index')->name('holiday.index');
                Route::get('/create', 'create')->name('holiday.create');
                Route::post('/store', 'store')->name('holiday.store');
                Route::post('/search', 'search')->name('holiday.search');
            });

        });





        Route::prefix('/leave')->group(function () {
            Route::controller(LeaveController::class)->group(function () {
                Route::get('/', 'index')->name('leave.index');
                Route::post('/store', 'store')->name('leave.store');
            });

        });


        Route::prefix('/weekend')->group(function () {
            Route::controller(WeekendController::class)->group(function () {
                Route::get('/', 'index')->name('weekend.index');
                Route::post('/store', 'store')->name('weekend.store');
            });
        });


        Route::prefix('/application')->group(function(){
            Route::controller(ApplicationController::class)->group(function (){
                Route::get('/leave','leaveCreate')->name('leavepdf.create');
                Route::post('/leave-outsider','leaveOutsiderPdf')->name('leavepdf.outsider');
            });
        });



        Route::prefix('/pdf')->group(function () {
            Route::controller(PDFController::class)->group(function () {
                Route::get('/employee-individual-report', 'empIndividual')->name('empIndividual.pdf');
                Route::get('/date-dept-report', 'dayWiseReport')->name('dayWiseReport.pdf');
                
            });
        });









    });

});


Route::get('/device', [DeviceController::class, 'test'])->name('test');


Route::get('/login2', function () {
    return view('guest.login');
});



Route::get('/forget2', function () {
    return view('guest.forget');
});



Route::prefix('/devices')->group(function () {
    Route::controller(DeviceController::class)->group(function () {
        Route::get('/', 'list')->name('device.list');
        Route::get('/add-new', 'create')->name('device.create');
        Route::post('/store', 'store')->name('device.store');
        Route::get('/device-status-check/{id}', 'checkDeviceStatus')->name('check.device.status');


    });


});



Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
