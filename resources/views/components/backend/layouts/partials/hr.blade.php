@props(['content'])

<div class="container mt-2 mb-3 d-none d-md-block">
    <div class="position-relative">
        <div class="border-top my-0"></div>
        <div class="text-center bg-white p-1 rounded"
            style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="d-none d-md-block text-mute" style="font-size: .9rem !important;">
                {{ $content }}
            </div>
        </div>
        <div class="border-top my-0"></div>
    </div>
</div>

<div class="container mb-2 mt-2 d-md-none">
    <hr>
    <div class="d-md-none text-muted" style="font-size: .6rem">
        {{ $content }}
    </div>
</div>
