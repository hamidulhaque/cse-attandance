@once
    <script>
        $(document).ready(function() {
            @if ($errors->any())
                Swal.fire({
                    icon: 'error',
                    title: 'OPPS!',
                    html: '<ul style="list-style-type: none;">' +
                        @foreach ($errors->all() as $error)
                            '<li>{{ $error }}</li>' +
                        @endforeach
                    '</ul>',
                });
            @endif

            @if (session('success'))

                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                Toast.fire({
                    icon: 'success',
                    title: '{{ session('success') }}'
                });
            @endif

        });
    </script>
@endonce
