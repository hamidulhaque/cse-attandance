<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">


        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard') }}">
                <i class="mdi mdi-grid-large menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>




        <li class="nav-item nav-category">Reports Preview</li>
        <li class="nav-item">
            <a class="nav-link {{ request()->routeIs('individualReport.View', 'daywise.view') ? 'active show' : '' }}"
                data-bs-toggle="collapse" href="#report"
                aria-expanded="{{ request()->routeIs('individualReport.View', 'daywise.view') ? 'true' : 'false' }}"
                aria-controls="report">
                <i class="menu-icon mdi mdi-file-document-outline"></i>
                <span class="menu-title">Reports</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse  {{ request()->routeIs('individualReport.View', 'daywise.view') ? 'show' : '' }}"
                id="report">
                <ul
                    class="nav flex-column sub-menu {{ request()->routeIs('individualReport.View', 'daywise.view') ? 'active' : '' }}">
                    <li
                        class="nav-item {{ request()->routeIs('individualReport.View') ? 'active' : '' }}">
                        <a class="nav-link {{ request()->routeIs('individualReport.View') ? 'active' : '' }}"
                            href="{{ route('individualReport.View') }}">Employee Report</a>
                    </li>
                    <li class="nav-item {{ request()->routeIs('daywise.view') ? 'active' : '' }}">
                        <a class="nav-link {{ request()->routeIs('daywise.view') ? 'active' : '' }}"
                            href="{{ route('daywise.view') }}">Day Wise Report</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#">Leave Report</a>
                    </li>

                </ul>
            </div>
        </li>









        <li class="nav-item nav-category">HRM</li>
        <li class="nav-item {{ request()->routeIs('employee.list') ? 'active show' : '' }}">
            <a class="nav-link" data-bs-toggle="collapse" href="#ui-basic"
                aria-expanded="{{ request()->routeIs('employee.list') ? 'true' : 'false' }}" aria-controls="ui-basic">
                <i class="menu-icon mdi mdi-account-group-outline"></i>
                <span class="menu-title">HRM</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse {{ request()->routeIs('employee.list') ? 'active show' : '' }}" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item {{ request()->routeIs('employee.list') ? 'active' : '' }}">
                        <a class="nav-link {{ request()->routeIs('employee.list') ? 'active' : '' }}"
                            href="{{ route('employee.list') }}">Employees</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">Departments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">Positions</a>
                    </li>

                </ul>
            </div>
        </li>






        <li class="nav-item {{ request()->routeIs('holiday.index', 'weekend.index') ? 'active show' : '' }}">

            <a class="nav-link {{ request()->routeIs('holiday.index',  'weekend.index') ? 'active show' : '' }}"
                data-bs-toggle="collapse" href="#schedules"
                aria-expanded="{{ request()->routeIs('holiday.index', 'weekend.index') ? 'true' : 'false' }}" aria-controls="schedules">

                <i class="menu-icon mdi mdi-calendar-clock"></i>
                <span class="menu-title">Schedules</span>
                <i class="menu-arrow"></i>

            </a>


            <div class="collapse {{ request()->routeIs('holiday.index', 'weekend.index') ? 'show' : '' }}" id="schedules">

                <ul class="nav flex-column sub-menu {{ request()->routeIs('holiday.index', 'weekend.index') ? 'active' : '' }}">
                    <li class="nav-item">
                        <a class="nav-link" href="">Shifts</a>
                    </li>
                    <li class="nav-item {{ request()->routeIs('weekend.index') ? 'active' : '' }}">
                        <a class="nav-link {{ request()->routeIs('weekend.index') ? 'active' : '' }}" href="{{ route('weekend.index') }}">Weekends</a>
                    </li>
                    <li class="nav-item {{ request()->routeIs('holiday.index') ? 'active' : '' }}">
                        <a class="nav-link {{ request()->routeIs('holiday.index') ? 'active' : '' }}"
                            href="{{ route('holiday.index') }}">Holidays</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">In/Out</a>
                    </li>
                </ul>

            </div>
        </li>








        <li class="nav-item {{ request()->routeIs('manualAttandance.view','leave.index') ? 'active show' : '' }}">
            <a class="nav-link " data-bs-toggle="collapse" href="#manualUpdate"
                aria-expanded="{{ request()->routeIs('manualAttandance.view','leave.index') ? 'true' : 'false' }}"
                aria-controls="manualUpdate">
                <i class="menu-icon mdi mdi-update"></i>
                <span class="menu-title">Manual Update</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse {{ request()->routeIs('manualAttandance.view','leave.index') ? 'active show' : '' }}"
                id="manualUpdate">
                <ul class="nav flex-column sub-menu {{ request()->routeIs('manualAttandance.view','leave.index') ? 'active' : '' }} ">
                    <li class="nav-item {{ request()->routeIs('manualAttandance.view') ? 'active' : '' }}">
                        <a class="nav-link {{ request()->routeIs('manualAttandance.view') ? 'active' : '' }}"
                            href="{{ route('manualAttandance.view') }}">Attendance</a>
                    </li>
                    <li class="nav-item {{ request()->routeIs('leave.index') ? 'active' : '' }}" >
                        <a class="nav-link {{ request()->routeIs('leave.index') ? 'active' : '' }}" href="{{ route('leave.index') }}">Leave</a>
                    </li>
                </ul>
            </div>
        </li>








        <li class="nav-item nav-category">Synchronizations</li>
        <li class="nav-item {{ request()->routeIs('deviceSync.view') ? 'active show' : '' }}">
            <a class="nav-link" data-bs-toggle="collapse" href="#sync"
                aria-expanded="{{ request()->is('deviceSync.view') ? 'true' : 'false' }}" aria-controls="sync">
                <i class="menu-icon icon-screen-smartphone"></i>
                <span class="menu-title">Device</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse {{ request()->routeIs('deviceSync.view') ? 'active show' : '' }}" id="sync">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item {{ request()->is('deviceSync.view') ? 'active' : '' }}">
                        <a class="nav-link {{ request()->is('deviceSync.view') ? 'active' : '' }}"
                            href="{{ route('deviceSync.view') }}">Sync</a>

                    </li>

                </ul>
            </div>
        </li>







        <li class="nav-item nav-category">Miscellaneous</li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#auth" aria-expanded="false"
                aria-controls="auth">
                <i class="menu-icon mdi mdi-account-circle-outline"></i>
                <span class="menu-title">Roles & Users</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="#"> Users </a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"> Roles & Permissions </a>

                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item nav-category">Applications</li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#application" aria-expanded="false"
                aria-controls="application">
                <i class="menu-icon mdi mdi-account-circle-outline"></i>
                <span class="menu-title">Application Forms</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="application">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('leavepdf.create') }}"> Leave Application </a>
                    </li>
                   
                </ul>
            </div>
        </li>








        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#settings" aria-expanded="false"
                aria-controls="settings">
                <i class="icon-settings menu-icon"></i>
                <span class="menu-title">Settings</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="settings">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="#"> Site Settings </a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('device.list') }}"> Devices </a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"> Activity Log </a>

                    </li>

                </ul>
            </div>
        </li>




    </ul>
</nav>
