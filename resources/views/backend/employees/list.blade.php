<x-backend.layouts.master>

    @section('header_message', 'Department Employee List')

    <div class="card">
        <div class="card-body">




            <div class="table-responsive">
                <table class="table table-sm table-borderless" id="employeeTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            {{-- <th>Image</th> --}}
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Nick Name</th>
                            <th>Emp ID</th>
                            <th>Department</th>
                            <th>Gender</th>
                            <th>Joining Date</th>
                            {{-- <th>Shift</th> --}}
                            {{-- <th>Status</th> --}}
                            <th>Action</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($employees as $employee)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                {{-- <td><img src="https://placehold.co/600x400" alt="" class="img-fluid rounded-circle"> --}}
                                {{-- </td> --}}
                                <td>{{ $employee->first_name ?? null }}</td>
                                <td>{{ $employee->last_name ?? null }}</td>
                                <td>{{ $employee->nickname ?? null }}</td>
                                <td>{{ $employee->emp_code ?? null }}</td>
                                <td>{{ $employee->department_id ?? null }}</td>
                                <td>{{ $employee->gender ?? null }}</td>
                                <td>{{ $employee->hiredate ?? null }}</td>

                                {{-- <td>Shift 2</td> --}}
                                {{-- <td><span class="badge badge-sm bg-success">Active</span></td> --}}
                                <td><a href="" class="btn btn-sm btn-primary"><i
                                            class="mdi mdi-square-edit-outline"></i></a></td>
                            </tr>
                        @endforeach

                    </tbody>


                </table>
            </div>
        </div>
    </div>




    @push('custom_js')
        <script>
            // new DataTable('#employeeTable');

            $(document).ready(function() {
                $('#employeeTable').DataTable();
            });
        </script>
    @endpush







</x-backend.layouts.master>
