<x-backend.layouts.master>
    @section('header_message', 'Leave Approve Sheet! ')

    @push('custom_css')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <style>
            .custom-table th,
            .custom-table td {
                font-size: 15px;
                padding: 4px 5px;
            }
        </style>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    @endpush
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-sm-flex justify-content-between align-items-start">
                        <div>
                            <h4 class="card-title">Leave Entries</h4>
                            <p class="card-description mb-0">Store leave for employees.</p>
                        </div>

                    </div>
                    <div class="card-body">
                        <form class="form-prevent" action="{{ route('leave.store') }}" method="POST">
                            @csrf
                            @method('post')
                            <div class="row ">

                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Employee ID</label>
                                        <input type="text" class="form-control  employee-autocomplete"
                                            placeholder="Ex. 218325" name="emp_code" value="{{ old('emp_code') }}"
                                            required>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="datepicker">Date</label>
                                        <div class="input-group date" id="datepicker">
                                            <input type="text" class="form-control" name="dates"
                                                placeholder="Select dates" value="{{ old('dates') }}"
                                                autocomplete="off" required>
                                            <span class="input-group-append">
                                                <span class="input-group-text bg-white">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Approved By</label>
                                        <input type="text" class="form-control approved" placeholder="Ex. 218325"
                                            name="approved" value="{{ old('approved') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Leave Type</label>
                                        <select class="form-select" name="leave_type" id="leavceType">
                                            @if (isset($leave_types))
                                                @foreach ($leave_types as $leave)
                                                    <option value="{{ $leave->id }}">{{ $leave->title }}</option>
                                                @endforeach
                                            @endif
                                        </select>

                                    </div>
                                </div>


                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Cause</label>
                                        <textarea class="form-control" name="cause" id="exampleTextarea1" rows="4" placeholder="Cause here">{{ old('cause') }}</textarea>
                                    </div>
                                </div>







                                <div class="col-md-2 col-sm-6 align-self-center">
                                    <button
                                        class="btn btn-primary btn-sm w-100 form-prevent-multiple-submit">Submit</button>
                                </div>




                            </div>


                        </form>
                    </div>




                </div>
            </div>
        </div>

    </div>




    @push('custom_js')
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <script src="{{ asset('backend/js/jquery-ui.js') }}"></script>
        <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
        <script>
            var empDropdownListRoute = "{{ route('emp.DropList') }}";
        </script>
        <script src="{{ asset('backend/js/manual-attandance.js') }}"></script>
        <script src="{{ asset('backend/js/leave-attandance.js') }}"></script>



        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js"
            integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>

        <script type="text/javascript">
            $(function() {
                $('#datepicker').datepicker({
                    todayHighlight: true,
                    startDate: null,
                    multidate: true,
                    format: "dd-mm-yyyy",
                    language: 'en'
                });
            });
        </script>
    @endpush








</x-backend.layouts.master>
