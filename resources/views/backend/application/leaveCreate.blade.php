<x-backend.layouts.master>
    @section('header_message', 'Leave Application! ')
    @push('custom_css')
        <style>
            hr.hr-text {
                position: relative;
                border: none;
                height: 1px;
                background: #999;
            }

            hr.hr-text::before {
                content: attr(data-content);
                display: block;
                background: #fff;
                font-weight: bold;
                font-size: 0.85rem;
                color: #999;
                border-radius: 30rem;
                padding: 0.2rem 2rem;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
            }
        </style>
    @endpush
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-sm-flex justify-content-between align-items-start">
                        <div>
                            <h4 class="card-title">Leave Application</h4>
                            <p class="card-description mb-0">Create leave application</p>
                        </div>
                        <div>
                            <a role="button" class="btn btn-sm btn-primary" href="#"> <span
                                    class="mdi mdi-download text-white"></span> Download Blank Form</a>
                        </div>

                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="text-danger">{{ $error }}</div>
                            @endforeach
                        @endif

                        <div class="row">

                            {{-- insider  --}}


                            {{-- <div class="col-12">
                                <h4>For Persons in this system.</h4>
                                <form class="form-prevent" id="newHoliday" action="#" method="POST">
                                    @csrf
                                    @method('post')
                                    <div class="row manual-entry p-3">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>Emp Code</label>
                                                <input type="text" class="form-control" placeholder="Ex: 160001"
                                                    name="emp_code" value="{{ old('emp_code') }}">
                                                @error('emp_code')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>From Date</label>
                                                <input type="date" class="form-control" name="fromDate"
                                                    value="{{ old('fromDate') }}">
                                                @error('fromDate')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>To Date</label>
                                                <input type="date" class="form-control" name="toDate"
                                                    value="{{ old('toDate') }}">
                                                @error('toDate')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror

                                            </div>
                                        </div>


                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>Leave type</label>
                                                <input type="text" class="form-control" name="remark"
                                                    value="{{ old('remark') }}" placeholder="Remarks">
                                                @error('remark')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                    </div>

                                    <div class="modal-footer d-flex"">
                                        <a role="button" class="btn btn-secondary"
                                            href="{{ route('holiday.index') }}">Back</a>
                                        <button type="submit"
                                            class="btn btn-success form-prevent-multiple-submit">Submit</button>
                                    </div>
                                </form>
                            </div> --}}



                            {{-- insider end --}}

                            {{-- outsider start  --}}


                            <div class="col-12">

                                <h4>Out-Sider</h4>

                                <form class="form-prevent" action="{{ route('leavepdf.outsider') }}" id="outsider" method="POST">

                                    @csrf
                                    @method('post')


                                    <div class="row manual-entry p-3">


                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" placeholder="Ex: John Doe"
                                                    name="Oname" value="{{ old('Oname') }}" required>
                                                @error('Oname')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>



                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>Emp ID</label>
                                                <input type="text" class="form-control" placeholder="Ex: 160001"
                                                    name="oEmpID" value="{{ old('oEmpID') }}" required>
                                                @error('OEmpID')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>



                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>Designation</label>
                                                <input type="text" class="form-control" placeholder="Ex: Programmer"
                                                    name="Odesg" value="{{ old('Odesg') }}" required>
                                                @error('Odesg')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>Phone</label>
                                                <input type="text" class="form-control" placeholder="Ex: John Doe"
                                                    name="Omoble" value="{{ old('Omoble') }}" required>
                                                @error('Omoble')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>



                                        <div class="col-md-4 col-sm-6">
                                            <div class="form-group">
                                                <label>Leave Type</label>
                                                <select name="OleaveType" class="form-select">
                                                    @foreach ($leave_types as $leave)
                                                        <option value="{{ $leave->id }}">{{ $leave->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>






                                        <div class="col-md-4 col-sm-6">
                                            <div class="form-group">
                                                <label>From Date</label>
                                                <input type="date" class="form-control" name="OfromDate"
                                                    value="{{ old('OfromDate') }}" required>
                                                @error('OfromDate')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="col-md-4 col-sm-6">
                                            <div class="form-group">
                                                <label>To Date</label>
                                                <input type="date" class="form-control" name="OtoDate"
                                                    value="{{ old('OtoDate') }}">
                                                @error('OtoDate')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror

                                            </div>
                                        </div>



                                        <div class="col-md-4 col-sm-6">
                                            <div class="form-group">
                                                <label>Correspondence Address</label>
                                                <input type="text" class="form-control" name="oCAddress"
                                                    value="{{ old('oCAddress') }}" placeholder="If out of city">

                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-6">
                                            <div class="form-group">
                                                <label>Reason</label>
                                                <input type="text" class="form-control" name="reason"
                                                    value="{{ old('reason') }}" placeholder="Your reason here"
                                                    required>

                                            </div>
                                        </div>




                                        <x-backend.layouts.partials.hr :content="' Details of taking Makeup Class (if there is any class schedule during
                                                                                                                                                                                                                                                                                        the leave period)'" />

                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <input type="date" class="form-control" name="OmakeUpdate"
                                                    value="{{ old('OmakeUpdate') }}">
                                                @error('OmakeUpdate')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>



                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>Time</label>
                                                <input type="time" class="form-control" name="OmakeUptime"
                                                    value="{{ old('OmakeUptime') }}">
                                                @error('OmakeUptime')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>


                                        <x-backend.layouts.partials.hr :content="' Details of the person who will carry out the duties
                                                                                                                                                                                                                                                in
                                                                                                                                                                                                                                                the absence of the incumbent'" />





                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>Person Name</label>
                                                <input type="text" class="form-control" name="oPname"
                                                    value="{{ old('oPname') }}" placeholder="Ex: John Doe">
                                                @error('oPname')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>



                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>Person Designation</label>
                                                <input type="text" class="form-control" name="OPDeg"
                                                    value="{{ old('OPDeg') }}" placeholder="Ex: Programmer">
                                                @error('OPDeg')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                    </div>

                                    <div class="modal-footer d-flex"">
                                        <button type="button" class="btn btn-danger" onclick="clearOutsiderForm()">Clear</button>
                                        <button type="submit"
                                            class="btn btn-primary form-prevent-multiple-submit">Generate PDF</button>
                                    </div>


                                </form>


                            </div>

                        </div>

                    </div>

                </div>


            </div>

        </div>

    </div>





    @push('custom_js')
    <script>
        function clearOutsiderForm() {
        var form = document.getElementById("outsider");
        form.reset();
    }
        </script>        
    @endpush








</x-backend.layouts.master>
