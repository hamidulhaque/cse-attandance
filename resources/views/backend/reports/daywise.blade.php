<x-backend.layouts.master>

    @section('header_message', 'individuals Employee Attandance Report')
    <meta name="csrf-token" content="{{ csrf_token() }}">


    @push('custom_css')
        <link rel="stylesheet" href="{{ asset('backend/vendors/select2/select2.min.css') }}">
        <style>
            .custom-table th,
            .custom-table td {
                font-size: 15px;
                /* Adjust the font size as needed */
                padding: 4px 5px;
                /* Adjust the padding as needed */
            }

            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                padding: 5px 5px !important;
                color: white !important;
            }

            .select2-container--default .select2-selection--multiple {
                border: 1px solid #dee2e6;
                font-weight: 400;
                font-size: 0.875rem;
                border-radius: 4px;
                height: 2rem;
            }
        </style>
    @endpush
   

    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Day-wise Report (All Employees)</h4>
                    <p class="card-description mb-0">Get day wise report for all employees.</p>
                    <div class="card-body">
                        <form class="form-prevent" action="{{ route('daywise.report') }}" method="POST">
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-md-3 col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label for="dept">Department</label> <br>
                                        <select class="js-example-basic-multiple w-100" name="dept-name[]"
                                            id="dept" multiple="multiple">
                                            @foreach ($departments as $dept)
                                                <option value="{{ $dept->dept_name }}">{{ $dept->dept_name }}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label for="fromDate">From Date</label>
                                        <input type="date" class="form-control" id="fromDate" name="from-date"
                                            value="{{ old('from-date') }}">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label for="toDate">To Date</label>
                                        <input type="Date" class="form-control" id="toDate" name="to-date"
                                            value="{{ old('to-date') }}">
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 mb-2 align-self-end">
                                    <div class="form-group">
                                        <button class="btn btn-primary btn-sm w-100 form-prevent-multiple-submit">Search</button>
                                    </div>
                                </div>
                            </div>


                        </form>

                    </div>

                    @if (session('additionalData'))
                        @php

                            $datas = session('additionalData');
                            $deptNames = session('deptNames');
                            $durations = session('duration');
                            $start_date = $durations['start_date'];
                            $end_date = $durations['end_date'];
                            $query_date = session('now');
                            
                        @endphp
                        <div class="home-tab">
                            <div class="d-sm-flex align-items-center justify-content-end border-bottom">
                                <div>
                                    <div class="btn-wrapper">

                                        <a href="{{ route('dayWiseReport.pdf', [
                                            'deptNames' => $deptNames,
                                            'start_date' => $start_date ,
                                            'end_date' => $end_date,
                                            'queryDate' => $query_date,
                                        ]) }}"
                                            target="_blank" class="btn btn-primary text-white me-0"><i
                                                class="icon-download"></i> PDF Export</a>
                                    </div>

                               
                                </div>
                            </div>

                        </div>




                        <div class="row mt-2">


                            <div class="col-12">
                                <div class="table-responsive">
                                    <table class="table table-sm table-bordered text-center  custom-table">
                                        <thead class="bg-primary text-white">
                                            <th>Date</th>
                                            <th>In Time</th>
                                            <th>Out Time</th>

                                            <th>Remarks</th>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    </div>

    @push('custom_js')
        <script src="{{ asset('backend/vendors/select2/select2.min.js') }}"></script>
        <script>
            (function($) {
                'use strict';

                if ($("#dept").length) {
                    $("#dept").select2();

                }

            })(jQuery);
        </script>
    @endpush




</x-backend.layouts.master>
