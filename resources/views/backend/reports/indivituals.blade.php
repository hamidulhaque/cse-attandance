<x-backend.layouts.master>

    @section('header_message', 'individuals Employee Attandance Report')
    <meta name="csrf-token" content="{{ csrf_token() }}">


    @push('custom_css')
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <style>
            .custom-table th,
            .custom-table td {
                font-size: 15px;
                /* Adjust the font size as needed */
                padding: 4px 5px;
                /* Adjust the padding as needed */
            }
        </style>
    @endpush
    {{-- @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif --}}

    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Employee Report (individuals)</h4>
                    <p class="card-description mb-0">Get individuals attandace report of an Employee</p>
                    <div class="card-body">
                        <form action="{{ route('individual.Reports') }}" method="POST">
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-md-3 col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label for="emp_id">Employee ID</label>
                                        <input type="text" class="form-control" id="emp_id"
                                            placeholder="Ex. 218325" name="emp-code" value="{{ old('emp-code') }}">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label for="fromDate">From Date</label>
                                        <input type="date" class="form-control" id="fromDate" name="from-date"
                                            value="{{ old('from-date') }}">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label for="toDate">To Date</label>
                                        <input type="Date" class="form-control" id="toDate" name="to-date"
                                            value="{{ old('to-date') }}">
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 mb-2 align-self-end">
                                    <div class="form-group">
                                        <button class="btn btn-primary btn-sm w-100">Search</button>
                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>


                    @if (session('additionalData'))
                        @php
                            $emp_code = session('employee')->emp_code;
                            $start_date = session('startDate');
                            $end_date = session('endDate');
                            $query_date = session('now');
                        @endphp
                        <div class="home-tab">
                            <div class="d-sm-flex align-items-center justify-content-end border-bottom">
                                <div>
                                    <div class="btn-wrapper">

                                        <a href="{{ route('empIndividual.pdf', [
                                            'employee' => $emp_code,
                                            // 'data' => session('additionalData'),
                                            'startDate' => $start_date,
                                            'endDate' => $end_date,
                                            'queryDate' => $query_date,
                                        ]) }}"
                                            target="_blank" class="btn btn-primary text-white me-0"><i
                                                class="icon-download"></i> PDF Export</a>
                                    </div>
                                </div>
                            </div>

                        </div>




                        <div class="row mt-2">

                            @if (session('employee'))
                                <div class="col-12 mb-3">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-borderless  custom-table">
                                            <tr>
                                                <th>Emp ID</th>
                                                <td>{{ session('employee')->emp_code }}</td>

                                                <th>Name</th>
                                                <td colspan="2">{{ session('employee')->first_name }}</td>


                                                <th>Designation</th>
                                                <td colspan="2">
                                                    {{ optional(session('employee')->position)->position_name }}</td>

                                            </tr>
                                            <tr>

                                                <th>Department</th>
                                                <td>{{ optional(session('employee')->department)->dept_name }}</td>

                                                <th>From Date</th>
                                                <td>{{ session('startDate') }}</td>


                                                <th>To Date</th>
                                                <td>{{ session('endDate') }}</td>


                                                <th>Query Time</th>
                                                <td>{{ session('now') }}</td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            @endif
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table class="table table-sm table-bordered text-center  custom-table">
                                        <thead class="bg-primary text-white">
                                            <th>Date</th>
                                            <th>In Time</th>
                                            <th>Out Time</th>
                                            <th>Status</th>
                                            <th>Remarks</th>
                                        </thead>
                                        <tbody>
                                            @foreach (session('additionalData') as $date => $punchData)
                                                @if ($date != '01-01-70')
                                                    <tr>
                                                        <td>{{ $date }}</td>

                                                        <td>{{ $punchData['first_punch'] ?? '-' }} </td>
                                                        <td>{{ $punchData['last_punch'] ?? '-' }} </td>
                                                        <td>
                                                            @php
                                                                if ($punchData['status'] == 0) {
                                                                    $status = 'Regular';
                                                                } else {
                                                                    $status = $punchData['status'];
                                                                }

                                                            @endphp
                                                            {{ $status }}
                                                        </td>

                                                        <td></td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    </div>

    @push('custom_js')
        <!-- Include jQuery UI library -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(document).ready(function() {
                $("#emp_id").autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: '{{ route('emp.DropList') }}', // Use the named route here
                            method: 'POST',
                            dataType: 'json',
                            data: {
                                term: request.term
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(data) {
                                response(data);
                            },
                            error: function() {
                                console.error('AJAX request failed');
                            }
                        });
                    },
                    minLength: 2,

                });
            });
        </script>
    @endpush




</x-backend.layouts.master>
