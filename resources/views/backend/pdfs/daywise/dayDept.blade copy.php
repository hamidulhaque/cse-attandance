<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">

    <style type="text/css">
        @font-face {
            font-family: 'Raleway-Bold';
            src: url('/fonts/Raleway-Bold.ttf') format('truetype');
        }

        .head {
            text-align: center;
            margin: 0;
            margin-bottom: 1rem;
            padding: 0;
        }

        .head p {
            margin: 0;
            padding: 0;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            font-size: .8rem;
        }

        th,
        td {
            border: 1px solid black;
            Min-width: 60px;
            text-align: center;
            padding: 2px;
        }

        .summary th {
            border: 1px solid black;
            Min-width: 60px;
            text-align: right;
            padding: 2px;
        }
    </style>

</head>

<body>

    @foreach ($data as $department => $dates)
        <div class="page-break">
            <div class="head">
                <img src="{{ asset('black-uu-logo.webp') }}" alt="UTTARA UNIVERSITY" style="max-width: 10rem;">
                <p>
                    <span style="font-size: 1.5rem;">ATTENDANCE REPORT</span><br>
                    <span class="font-size: .8 rem;">(DATE & DEPARTMENT)</span>
                </p>
            </div>

            <div style="margin-bottom: 1rem;">
                <table>
                    <tr>
                        <th>Department</th>
                        <td colspan="2">{{ $department }}</td>

                        <th>Time Period</th>
                        <td colspan="2">
                            {{ $startDate }} <br> {{ $endDate }}
                        </td>

                        <th>Duration</th>
                        <td>
                            {{ \Carbon\Carbon::parse($endDate)->diffInDays(\Carbon\Carbon::parse($startDate)) + 1 }}
                            {{ \Carbon\Carbon::parse($endDate)->diffInDays(\Carbon\Carbon::parse($startDate)) + 1 > 1 ? 'days' : 'day' }}
                        </td>
                        <th>Query Date</th>
                        <td>{{ $now }}</td>
                    </tr>
                </table>
            </div>

            <div style="margin-bottom: .6rem;">
                @foreach ($dates as $date => $employees)
                    <div style="margin-bottom: .6rem;">
                        
                        <table style="margin-bottom: .4rem">
                            <tr style="background-color : rgb(185, 181, 181) ">
                                <th>Date:</th>
                                <td>{{ $date }}</td>

                                <th>Weekday</th>
                                <td>Sunday</td>
                                <th>Total Emp</th>
                                <td>{{ count($employees) }}</td>
                            </tr>
                        </table>

                        <table>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Scheduled In Time</th>
                                    <th>Check In</th>
                                    <th>Delay In</th>
                                    <th>Status</th>
                                    <th>Scheduled Out Time</th>
                                    <th>Check Out</th>
                                    <th>Early Out</th>
                                    <th>Working Hours</th>
                                </tr>
                            </thead>
                            {{-- @dd($employees) --}}
                            <tbody>
                                @foreach ($employees as $empid => $emp)
                                    <tr>
                                        <td>{{ $empid }}</td>
                                        <td>{{ $emp['name'] }}</td>
                                        <td>{{ $emp['position'] }}</td>
                                        <td>9:00 AM</td>
                                        <td>{{ $emp['first_punch'] ?? '-' }}</td>
                                        <td>-</td>
                                        <td>N/A</td>
                                        <td>5:00 PM</td>
                                        <td>{{ $emp['last_punch'] ?? '-' }}</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endforeach


            </div>

            <!-- Add the summarized section here if needed -->
            @unless ($loop->last)
                <div class="department" style="page-break-before: always;">
                </div>
            @endunless

        </div>
    @endforeach

</body>

</html>
