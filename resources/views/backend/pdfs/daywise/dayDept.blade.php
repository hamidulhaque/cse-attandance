<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">

    <style type="text/css">
        @font-face {
            font-family: 'Raleway-Bold';
            src: url('/fonts/Raleway-Bold.ttf') format('truetype');
        }

        .main-container {
            text-align: center;
        }

        .head {
            text-align: center;
            margin: 0;
            margin-bottom: 1rem;
            padding: 0;
        }

        .head p {
            margin: 0;
            padding: 0;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            font-size: .8rem;
            margin: auto;
        }

        th,
        td {
            border: 1px solid black;
            min-width: 60px;
            text-align: center;
            padding: 2px;
        }

        .summary th {
            border: 1px solid black;
            Min-width: 60px;
            text-align: right;
            padding: 2px;
        }


        .data_table th {

            min-width: 45px !important;
            padding: 4px !important;
            font-size: .8rem;
        }


        .data_table td {
            min-width: 45px !important;
            padding: 3px !important;
        }
    </style>


</head>

<body>

    @foreach ($data as $department => $dates)
        <div class="main-container">
            <div class="head">
                <img src="{{ asset('black-uu-logo.webp') }}" alt="UTTARA UNIVERSITY" style="max-width: 9rem;">
                <p>
                    <span style="font-size: 1.2rem;">ATTENDANCE REPORT</span><br>
                    <span class="font-size: .8 rem;">(DATE & DEPARTMENT)</span>
                </p>
            </div>

            <div class="main-container" style="margin-bottom: 1rem;">
                <table>
                    <tr>
                        <th>Department</th>
                        <td colspan="2">{{ $department }}</td>

                        <th>Time Period</th>
                        <td colspan="2">
                            {{ $startDate }} <br> {{ $endDate }}
                        </td>

                        <th>Duration</th>
                        <td>
                            {{ \Carbon\Carbon::parse($endDate)->diffInDays(\Carbon\Carbon::parse($startDate)) + 1 }}
                            {{ \Carbon\Carbon::parse($endDate)->diffInDays(\Carbon\Carbon::parse($startDate)) + 1 > 1 ? 'Days' : 'Day' }}
                        </td>
                        <th>Query Date</th>
                        <td>{{ $now }}</td>
                    </tr>
                </table>
            </div>

            <div style="margin-bottom: .6rem;">
                @foreach ($dates as $date => $employees)
                    <div style="margin-bottom: .6rem;">

                        <table style="margin-bottom: .4rem">
                            <tr style="background-color : rgb(214, 213, 213) ">
                                <th>Date:</th>
                                <td>{{ $date }}</td>

                                <th>Weekday</th>
                                <td>{{ \Carbon\Carbon::parse($date)->format('l') }}</td>

                                <th>Total Employees</th>
                                <td>{{ count($employees) }}</td>
                            </tr>
                        </table>

                        <table class="data_table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th style="font-size: .7rem; padding: .2rem !important;">Scheduled In Time</th>
                                    <th>Check In</th>
                                    <th>Delay In</th>
                                    <th>Status</th>
                                    <th>Scheduled Out Time</th>
                                    <th>Check Out</th>
                                    <th>Early Out</th>
                                    <th>Working Hours</th>
                                </tr>
                            </thead>
                            {{-- @dd($employees) --}}
                            <tbody>
                                @foreach ($employees as $empid => $emp)
                                    <tr>
                                        {{-- @dd($emp) --}}
                                        <td>{{ $empid }}</td>
                                        <td style="font-size: .7rem; padding: .2rem !important;">{{ $emp['name'] }}
                                        </td>
                                        <td style="font-size: .7rem; padding: .2rem !important;">{{ $emp['position'] }}
                                        </td>
                                        <td style="font-size: .7rem; padding: .2rem !important;">9:00 AM</td>
                                        <td style="font-size: .7rem; padding: .2rem !important;">
                                            {{ $emp['first_punch'] ?? '-' }} </td>


                                        <td>
                                            @if (isset($emp['first_punch']))
                                                @php
                                                    $scheduledTime = strtotime('09:00 AM');
                                                    $actualTime = strtotime($emp['first_punch']);

                                                    $difference = ($scheduledTime - $actualTime) / 60;

                                                    if ($difference >= 0) {
                                                        $formattedLateness = 'On time';
                                                    } else {
                                                        $delayedMins = $difference * -1;
                                                        if ($delayedMins >= 60) {
                                                            $hours = floor($delayedMins / 60);
                                                            $minutes = $delayedMins % 60;
                                                            $formatted_duration = sprintf('%02d.%02d', $hours, $minutes) . ' Hrs';
                                                            $formattedLateness = $formatted_duration;
                                                        } else {
                                                            $formattedLateness = $delayedMins . ' Min';
                                                        }
                                                    }

                                                @endphp
                                                {{ $formattedLateness }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @php
                                                if ($emp['holiday'] == null) {
                                                    $status = 'Regular';
                                                } else {
                                                    $status = 'Weekend';
                                                }
                                            @endphp
                                            {{ $status }}

                                        </td>
                                        <td>5:00 PM</td>
                                        <td style="font-size: .7rem; padding: .2rem !important;">
                                            {{ $emp['last_punch'] ?? '-' }}</td>
                                        <td style="font-size: .7rem; padding: .2rem !important;">

                                            @if (isset($emp['last_punch']))
                                                @php
                                                    $scheduledTime = strtotime('05:00 PM');
                                                    $actualTime = strtotime($emp['last_punch']);

                                                    $difference = ($scheduledTime - $actualTime) / 60;

                                                    if ($difference >= 0) {
                                                        if ($difference >= 60) {
                                                            $hours = floor($difference / 60);
                                                            $minutes = $difference % 60;
                                                            $formatted_duration = sprintf('%02d.%02d', $hours, $minutes) . ' Hrs';
                                                            $formatedEarlyMins = $formatted_duration;
                                                        } else {
                                                            $formatedEarlyMins = $difference . ' Min';
                                                        }
                                                    } else {
                                                        $formatedEarlyMins = 'On time';
                                                    }

                                                @endphp
                                                {{ $formatedEarlyMins }}
                                            @else
                                                -
                                            @endif

                                        </td>
                                        <td>
                                            @if (isset($emp['first_punch']) && isset($emp['last_punch']))
                                                @php
                                                    $inTimeMins = strtotime($emp['first_punch']);
                                                    $outTimeMins = strtotime($emp['last_punch']);

                                                    $differenceInWH = ($outTimeMins - $inTimeMins) / 60; // Corrected calculation

                                                    if ($differenceInWH >= 60) {
                                                        $hours = floor($differenceInWH / 60);
                                                        $minutes = $differenceInWH % 60;
                                                        $formatted_duration = sprintf('%02d.%02d', $hours, $minutes) . ' Hrs';
                                                        $workingHours = $formatted_duration;
                                                    } else {
                                                        $workingHours = $differenceInWH . ' Min';
                                                    }
                                                @endphp
                                                {{ $workingHours }}
                                            @else
                                                -
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endforeach


            </div>

            <!-- Add the summarized section here if needed -->
            @unless ($loop->last)
                <div class="department" style="page-break-before: always;">
                </div>
            @endunless

        </div>
    @endforeach

</body>

</html>
