<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">


    <style type="text/css">
        @font-face {
            font-family: 'Raleway-Bold';
            src: url('/fonts/Raleway-Bold.ttf') format('truetype');
        }


        .head {
            text-align: center;
            margin: 0;
            margin-bottom: 1rem;
            padding: 0;
        }

        .head p {
            margin: 0;
            padding: 0;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            font-size: .8rem;
        }

        th,
        td {
            border: 1px solid black;
            Min-width: 60px;
            text-align: center;
            padding: 2px;
        }

        .summary th {
            border: 1px solid black;
            Min-width: 60px;
            text-align: right;
            padding: 2px;
        }
    </style>
    <style>
        /* Add any additional styles you need */
        .page-number:before {
            content: counter(page);
        }

        .page-count:before {
            content: counter(pages);
        }
    </style>
</head>

<body>

    <div class="head">
        <img src="{{ asset('black-uu-logo.webp') }}" alt="UTTARA UNIVERSITY" style="max-width: 10rem;">
        <!-- <p style="font-size: 2rem;">UTTARA UNIVERSITY</p> -->
        <p style="font-size: 1.5rem; ">ATTENDANCE REPORT (INDIVIDUAL) </p>
    </div>
    <div style="margin-bottom: 1rem;">
        <table>
            <tr>
                <th>Emp ID</th>
                <td>{{ $employee->emp_code }}</td>

                <th>Name</th>
                <td colspan="2">{{ $employee->first_name . ' ' . $employee->last_name }}</td>


                <th>Designation</th>
                <td colspan="2">{{ $employee->position->position_name }}</td>

            </tr>
            <tr>

                <th>Department</th>
                <td>{{ $employee->department->dept_name }}</td>

                <th>From Date</th>
                <td>{{ $startDate }}</td>


                <th>To Date</th>
                <td>{{ $endDate }}</td>


                <th>Print Date</th>
                <td>{{ \Carbon\Carbon::parse($queryDate)->format('d-m-Y h:i A') }}</td>


            </tr>
        </table>
    </div>

    <div style="margin-bottom: .6rem;">
        <table class="">
            <thead>
                <th>Date</th>
                <th>Weekday</th>
                <th>Schedule In Time</th>
                <th>Check In</th>
                <th>Delay Time</th>
                <th>Status</th>
                <th>Schedule Out Time</th>
                <th>Check Out</th>
                <th>Early Out</th>
                <th>Working Hours</th>
            </thead>
            <tbody>
                @php
                    $totalDelayDays = 0;
                    $totalEarlyOutDays = 0;

                    $totalDelayInMins = 0;
                    $totalEarlyOutMins = 0;

                    $totalWorkingMins = 0;

                @endphp


                @foreach ($additionalData as $date => $punchData)
                    <tr>
                        <td>{{ $date }}</td>
                        <td>{{ $punchData['weekday'] }}</td>
                        <td>09:00 AM</td>
                        <td>{{ $punchData['first_punch'] ?? '-' }} </td>
                        <td>{{ $punchData['delayTime'] }}</td>
                        <td>
                            @php
                                if ($punchData['status'] == 0) {
                                    $status = 'Regular';
                                } else {
                                    $status = $punchData['status'];
                                }
                            @endphp
                            {{ $status }}
                        </td>
                        <td>05:00 PM</td>
                        <td>{{ $punchData['last_punch'] ?? '-' }}</td>

                        <td>{{ $punchData['earlyOut'] }}</td>
                        <td>{{ $punchData['workingHour'] }}</td>

                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>

    <div><span style="font-size: .8rem; text-decoration: underline; font-weight: bold; margin-bottom: .3rem;">Summarized
            Report For Total {{ \Carbon\Carbon::parse($startDate)->diffInDays(\Carbon\Carbon::parse($endDate)->addDay()) + 1 }}
            {{ \Carbon\Carbon::parse($startDate)->diffInDays(\Carbon\Carbon::parse($endDate)->addDay()) > 1 ? 'Days' : 'Day' }}:
        </span></div>
    <div style="margin-top: .4rem;">

        <table class="summary">


            <tr>

                <th>Working Days</th>
                <td>{{ $summary['workDays'] }} </td>

                <th>Manual Entries</th>
                <td>0 Day</td>

                <th>Offical Hours</th>
                <td>{{ $summary['officialWorkingHr'] }} </td>

            </tr>




            <tr>
                <th>Delay (Days)</th>
                <td>{{ $summary['totalDelayDays'] }}
                </td>
                <th>Delay (Hrs)</th>
                <td>
                    {{ $summary['totalDelayed'] }}
                </td>
                <th>Working Hours</th>
                <td>
                    {{ $summary['totalWorkingHr'] }}
                </td>

            </tr>


            <tr>
                <th>Early Out (Days)</th>
                <td>{{ $summary['totalEarlyOutDays'] }}</td>
                <th>Early Out (Hrs)</th>
                <td>
                    {{ $summary['totalEarlyOutHrs'] }}
                </td>

                <th>Due Hours</th>
                <td>

                    {{ $summary['dueHours'] }}
                </td>

            </tr>

            <tr>
                <th>Weekend</th>
                <td>{{ $summary['weekends'] }}</td>
                <th>Holidays</th>
                <td>{{ $summary['holidays'] }}</td>
                <th>Working Percentage</th>
                <td>
                    {{ $summary['percentage'] }}
                </td>

            </tr>
            <tr>
                <th>Leave</th>
                <td>{{ $summary['totalLeave'] }}</td>
                <th>Absent</th>
                <td>{{ $summary['totalAbsent'] }}</td>
                <th>Remark</th>
                <td>
                    {{ $summary['remark'] }}
                </td>
            </tr>
        </table>
        <div style="margin-top: .5rem; font-size: .8rem;">
            <span>NOTE: </span> <span>This is a computer-generated document. No signature is required.</span>
        </div>
    </div>

    <footer>
        {{-- {{ counter($page) }} --}}
    </footer>

</body>

</html>
