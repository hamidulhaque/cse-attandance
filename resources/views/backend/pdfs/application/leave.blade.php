<!doctype html>
<html lang="en">

<head>
    <title>Leave Form</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS v5.2.1 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous" />
    <style>
        .mt-8 {
            margin-top: 4rem;
        }

        * {
            margin-top: 0px;
        }
    </style>
</head>

<body>
    <h1 class="text-center m-0 p-0">Uttara University</h1>
    <p class="text-center m-0 p-0">Application Form for Casual Leave / Earned Leave /Other Leave <br>
        (Teachers and Officers)</p>
    <p class="m-0 p-0 text-end">Date:</p>
    <p class="m-0 p-0 mb-2 fst-italic" style="text-align: justify;">(Leave is not a matter of right. Leave can only be
        availed of after prior sanction by the appropriate authority.
        If the leave is not granted, salary will be deducted for the period of absence and the continuity of service
        will be affected. Leave Rules 2006 amended up to 16<sup>th</sup> Syndicate Meeting will be applicable for
        sanction of
        leave.)</p>
    <div class="row">
        <div class="col-12  p-0 m-0">
            <div class="table-responsive  p-0 m-0">
                <table class="t1 table table-sm table-borderless p-0 m-0">
                    <tr>
                        <th class="align-middle text-start">Employee/Teacher ID:</th>
                        <td class="align-middle text-center">16002</td>
                        <th class="align-middle text-start">Name:</th>
                        <td class="align-middle text-center"></td>
                    </tr>
                    <tr>
                        <th class="align-middle text-start">Designation ID:</th>
                        <td class="align-middle"></td>
                        <th class="align-middle text-start">Department/Office:</th>
                        <td class="align-middle"></td>
                    </tr>
                    <tr>
                        <th class="align-middle text-start">Mobile No:</th>
                        <td class="align-middle"></td>
                        <th class="align-middle text-start">Mode of Leave:</th>
                        <td class="align-middle" style="font-size: .8rem">
                            <x-check />Casual Leave/Earned Leave/Other Leave
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-12 p-0 m-0">
            <div class="table-responsive  p-0 m-0">
                <table class="table table-sm ">
                    <tr>
                        <th>Date: From</th>
                        <td></td>
                        <th>to</th>
                        <td></td>
                        <th> =Total leave</th>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-12  p-0 m-0">
            <div class="table-responsive">
                <table class="table table-sm ">
                    <tr>
                        <th>Reason for leave:</th>
                        <td></td>

                    </tr>
                </table>
            </div>
        </div>



        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-sm ">
                    <tr>
                        <th>Correspondence Address (if the person leaves the station during the leave period):</th>

                    </tr>
                    <tr>

                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-sm ">
                    <tr>
                        <th>Details of taking Makeup Class (if there is any class schedule during the leave period):
                        </th>

                    </tr>

                </table>
            </div>
        </div>
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-sm ">
                    <tr>
                        <th>Date:</th>
                        <td></td>
                        <th>Time:</th>
                        <td></td>
                    </tr>

                </table>
            </div>
        </div>


        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-sm ">
                    <tr>
                        <th>Details of the person who will carry out the duties in the absence of the incumbent:</th>

                    </tr>

                </table>
            </div>
        </div>
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-sm ">
                    <tr>
                    <tr>
                        <th>Name: </th>
                        <td></td>
                        <th>Designation:</th>
                        <td></td>
                        <th>Signature:</th>
                        <td></td>
                    </tr>


                    </tr>

                </table>
            </div>
        </div>
        <div class="col-12 mt-8 mb-2">
            <p class="text-end fw-bold">Signature of Applicant</p>
        </div>

        <div class="col-12">
            <span> <b>Opinion and recommendation of the Dean/Chairman/Head of the
                    office</b>.....................................................</span><br><br>
            <p>...............................................................................................................................................................................
            </p>
        </div>

        <div class="col-12" style="margin-top: 5rem !important">
            <p class="text-end fw-bold"> Signature of the Dean/Chairman/Head of the office</p>
        </div>
        <div class="col-12">
            <p class="fw-bold">Comments from the Registrar’s Office:</p>
        </div>

        <p class="m-0 p-0 lh-base fst-italic" style="text-align: justify;">The above person has enjoyed............days
            C.L............days E.L/other leave/ has not enjoyed any leave during the year.................and he/she
            may be granted leave for .................days / may not be granted any leave.</p>


        <div class="col-12 text-center" style="margin-top: 6rem!important;">
            <table class="table table-sm text-center">

                <tr>
                    <th> Authorized Officer <br>

                        Registrar’s Office</th>
                    <th> Registrar
                        <br>
                        Uttara University
                    </th>
                </tr>
            </table>
            <div>

            </div>
            <div>



            </div>
        </div>

    </div>

</body>

</html>
