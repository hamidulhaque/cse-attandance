<x-backend.layouts.master>
    @section('header_message', 'Holiday Entries! ')

    @push('custom_css')
        <style>
            .custom-table th,
            .custom-table td {
                font-size: 15px;
                /* Adjust the font size as needed */
                padding: 4px 5px;
                /* Adjust the padding as needed */
            }

            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                padding: 5px 5px !important;
                color: white !important;
            }

            .select2-container--default .select2-selection--multiple {
                border: 1px solid #dee2e6;
                font-weight: 400;
                font-size: 0.875rem;
                border-radius: 4px;
                height: 2rem;
            }
        </style>
    @endpush
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-sm-flex justify-content-between align-items-start">
                        <div>
                            <h4 class="card-title">Holiday Entries</h4>
                            <p class="card-description mb-0">Show holiday entries.</p>
                        </div>
                        <div>
                            <a role="button" class="btn btn-sm btn-primary" href="{{ route('holiday.create') }}">
                                Add new
                            </a>

                        </div>
                    </div>
                    <div class="card-body">
                        <form class="form-prevent" action="{{ route('holiday.search') }}" method="POST">
                            @csrf
                            @method('post')
                            <div class="row manual-entry">


                                <div class="col-md-2 col-sm-6">
                                    <div class="form-group">
                                        <label>From Date</label>
                                        <input type="date" class="form-control" name="fromDate"
                                            value="{{ old('fromDate') }}">
                                    </div>
                                </div>


                                <div class="col-md-2 col-sm-6">
                                    <div class="form-group">
                                        <label>To Date</label>
                                        <input type="date" class="form-control" name="toDate"
                                            value="{{ old('toDate') }}">
                                    </div>
                                </div>



                                <div class="col-md-2 col-sm-6 align-self-center">
                                    <button
                                        class="btn btn-primary btn-sm w-100 form-prevent-multiple-submit">Search</button>
                                </div>




                            </div>


                        </form>
                    </div>

                    @if (isset($holidays))
                        @if ($holidays->isNotEmpty())

                            <h4>Search Results:</h4>

                            <div class="home-tab">
                                <div class="d-sm-flex align-items-center justify-content-end border-bottom">
                                    <div>
                                        <div class="btn-wrapper">

                                            <a href="#" target="_blank" class="btn btn-primary text-white me-0"><i
                                                    class="icon-download"></i> PDF Export</a>
                                        </div>
                                    </div>
                                </div>

                            </div>




                            <div class="row mt-2">


                                <div class="col-12">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-bordered text-center  custom-table">
                                            <thead class="bg-primary text-white">
                                                <th>Title</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Remarks</th>
                                                <th>Duration</th>
                                                <th>By</th>

                                            </thead>

                                            <tbody>
                                                @foreach ($holidays as $holiday)
                                                    <tr>
                                                        <td>{{ $holiday->title }}</td>
                                                        <td> {{ \Carbon\Carbon::parse($holiday->from_date)->format('d-m-Y') }}
                                                        </td>
                                                        <td> {{ $holiday->to_date ? \Carbon\Carbon::parse($holiday->to_date)->format('d-m-Y') : '-' }}

                                                        </td>
                                                        <td>{{ $holiday->remark }}</td>
                                                        <td>

                                                            @if ($holiday->to_date)
                                                            {{ (\Carbon\Carbon::parse($holiday->from_date)->diffInDays(\Carbon\Carbon::parse($holiday->to_date))) + 1 }}
                                                                days
                                                            @else
                                                                1 day
                                                            @endif
                                                        </td>
                                                        <td>{{ $holiday->createdBy->name }}</td>
                                                    </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        @else
                            <p>No holidays found for the specified date range.</p>
                        @endif
                    @endif




                </div>
            </div>
        </div>

    </div>












</x-backend.layouts.master>
