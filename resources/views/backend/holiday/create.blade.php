<x-backend.layouts.master>
    @section('header_message', 'Holiday Entries! ')
    @push('custom_css')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <style>
            .modal-header {
                border-bottom: none !important;
            }

            .modal-footer {
                border-top: none !important;
            }
        </style>
    @endpush
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-sm-flex justify-content-between align-items-start">
                        <div>
                            <h4 class="card-title">Holiday Entry</h4>
                            <p class="card-description mb-0">Give Or Show holiday entries.</p>
                        </div>

                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="text-danger">{{ $error }}</div>
                            @endforeach
                        @endif
                        <form class="form-prevent" id="newHoliday" action="{{ route('holiday.store') }}" method="POST">
                            @csrf
                            @method('post')
                            <div class="row manual-entry p-3">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" class="form-control" placeholder="Ex. May Day"
                                            name="title" value="{{ old('title') }}">
                                        @error('title')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>


                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>From Date</label>
                                        <input type="date" class="form-control" name="fromDate"
                                            value="{{ old('fromDate') }}">
                                        @error('fromDate')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>


                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>To Date</label>
                                        <input type="date" class="form-control" name="toDate"
                                            value="{{ old('toDate') }}">
                                        @error('toDate')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror

                                    </div>
                                </div>


                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Remark</label>
                                        <input type="text" class="form-control" name="remark"
                                            value="{{ old('remark') }}" placeholder="Remarks">
                                        @error('remark')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer d-flex"">
                                <a role="button" class="btn btn-secondary"
                                    href="{{ route('holiday.index') }}">Back</a>
                                <button type="submit"
                                    class="btn btn-success form-prevent-multiple-submit">Submit</button>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>

    </div>









    @push('custom_js')
        <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
    @endpush







</x-backend.layouts.master>
