<x-backend.layouts.master>
    @section('header_message', 'Weekend Update Sheet! ')


    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-sm-flex justify-content-between align-items-start">
                        <div>
                            <h4 class="card-title">Weekend Entries</h4>
                            <p class="card-description mb-0">Store weekend /off day for employees.</p>
                        </div>

                    </div>
                    <div class="card-body">
                        <form class="form-prevent" action="{{ route('weekend.store') }}" method="POST">
                            @csrf
                            @method('post')
                            <div class="row ">

                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Department *</label>
                                        <select class="form-select" name="department" id="department">
                                            <option value="all">All</option>
                                            @if (isset($departments))
                                                @foreach ($departments as $dept)
                                                    <option value="{{ $dept->dept_code }}">{{ $dept->dept_name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Week Days *</label>
                                        <select class="form-select" name="weekday" id="weekday">
                                            @if (isset($weekdays))
                                                @foreach ($weekdays as $day)
                                                    <option value="{{ $day->id }}">{{ $day->title }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Announced Date</label>
                                        <input type="date" name="date" id="date" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Remarks</label>
                                        <input type="text" name="remark" id="remark" class="form-control" placeholder="remarks">
                                    </div>
                                </div>
                                

                                <div class="col-md-2 col-sm-6 align-self-center">
                                    <button
                                        class="btn btn-primary btn-sm w-100 form-prevent-multiple-submit">Submit</button>
                                </div>




                            </div>


                        </form>
                    </div>



                </div>
            </div>
        </div>

    </div>









</x-backend.layouts.master>
