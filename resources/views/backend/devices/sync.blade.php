<x-backend.layouts.master>
    @section('header_message', 'Device Syncronize & Log')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">

                <div class="card-body">
                    <h4 class="card-title">Sync Device</h4>
                    <p class="card-description">Sync Device Manually!</p>
                    <div class="template-demo">
                        <form id="syncForm" action="{{ route('sync:biotime-data') }}" method="POST">
                            @csrf
                            <button type="button" class="btn btn-primary" id="syncButton">Sync Device Now <i
                                    class="mdi mdi-sync"></i></button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-body">
                            <h4 class="card-title">Previous Logs</h4>
                            <p class="card-description">last logs</p>
                            <div class="template-demo">
                                <div class="table-responsive">

                                    <table class="table table-sm table-borderless table-hover">

                                        @if ($logs->isEmpty())
                                            <thead>
                                                <tr>
                                                    <th>No Logs Found</th>
                                                </tr>
                                            </thead>
                                        @else
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Message</th>
                                                    <th>Status</th>
                                                    <th>At</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($logs as $log)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{!! Str::limit($log->message, 50, ' ...') !!}</td>
                                                        <td>
                                                            @if ($log->status)
                                                                <span class="badge badge-sm bg-success">Success</span>
                                                            @else
                                                                <span class="badge badge-sm bg-danger">Error</span>
                                                            @endif
                                                        </td>
                                                        <td>{{ $log->created_at->diffForHumans() }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        @endif

                                    </table>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    @push('custom_js')
        <script>
            $(document).ready(function() {
                $('#syncButton').click(function() {
                    console.log('clicked')
                    $(this).prop('disabled', true);
                    $(this).html('Syncing <i class="mdi mdi-spin mdi-loading"></i>');
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('sync:biotime-data') }}',
                        data: {
                            _token: '{{ csrf_token() }}'
                        },
                        success: function(response) {
                            location.reload();
                        },
                        error: function(error) {
                            $('#syncButton').html('Sync Device Now <i class="mdi mdi-sync"></i>');
                            $('#syncButton').prop('disabled', false);
                        }
                    });
                });
            });
        </script>
    @endpush
</x-backend.layouts.master>
