<x-backend.layouts.master>
    @section('header_message', 'Add New Attandace Devices')

    <div class="card">
        <div class="card-body">
            <div class="container">
                <div class="container-fluid">
                    <div class="mx-auto mb-2">
                        <div class="text-center mb-2">
                            <h2>Add A New Device</h2>
                        </div>

                    </div>

                    @if ($errors->any())
                        {{ implode('', $errors->all('<div>:message</div>')) }}
                    @endif

                    <div class="row">

                        <form action="{{ route('device.store') }}" method="POST">
                            @csrf
                            @method('post')

                            <div class="form-group">
                                <label for="exampleInputUsername1">Title</label>
                                <input type="text" name="name" value="{{ old('name') }}" class="form-control" id="title"
                                    placeholder="Device Title">
                            </div>
                            <div class="form-group">
                                <label for="address">Device Address</label>
                                <input type="text" name="address" value="{{ old('address') }}" class="form-control" id="address"
                                    placeholder="Device Address">
                            </div>
                            <div class="form-group">
                                <label for="port">Device Port</label>
                                <input type="text" name="port" value="{{ old('port') }}" class="form-control" id="port"
                                    placeholder="Device Port">
                            </div>
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" name="username" value="{{ old('username') }}" class="form-control" id="username"
                                    placeholder="Device Username">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" id="password" placeholder="Password"
                                    name="password">
                            </div>


                            <button type="submit" class="btn btn-primary me-2">Submit</button>

                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>


</x-backend.layouts.master>
