<x-backend.layouts.master>
    @section('header_message', 'Attandace Devices')

    <div class="card">
        <div class="card-body">
            <div class="container">
                <div class="container-fluid">
                    <div class="mx-auto">
                        <div class="text-center mb-2">
                            <h2>Device List</h2>
                        </div>

                        <div class="d-flex align-items-top justify-content-end mb-2">
                            <a role="button" href="{{ route('device.create') }}" class="btn btn-sm btn-primary btn-icon-text">
                                <i class="ti-plus btn-icon-prepend"></i>
                                New
                            </a>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-sm table-hover">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Port</th>
                                        <th>Username</th>
                                        <th>Status</th>
                                        <th>At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (empty($devices))
                                        <tr class="text-center">
                                            <th colspan="8">No Data Found</th>
                                        </tr>
                                    @else
                                        @foreach ($devices as $device)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $device->name }}</td>
                                                <td>{{ $device->address }}</td>
                                                <td>{{ $device->port }}</td>
                                                <td>{{ $device->username }}</td>
                                                <td >
                                                <span id="status_{{ $device->id }}" class="badge badge-sm"></span></td>
                                                <td>{{ $device->created_at }}</td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('custom_js')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script>
        $(document).ready(function() {
            checkAllDeviceStatus();
        });
    
        function checkAllDeviceStatus() {
            @foreach ($devices as $device)
                checkDeviceStatus({{ $device->id }});
            @endforeach
        }
    
        function checkDeviceStatus(deviceId) {
            $.ajax({
                url: `/devices/device-status-check/${deviceId}`,
                method: 'GET',
                dataType: 'json',
                success: function(data) {
                    const statusElement = $(`#status_${deviceId}`);
                    statusElement.text(data.status);
                    console.log(data.status);
                    statusElement.addClass(data.status === 'running' ? 'badge-success' :
                        'badge-danger');
                },
                error: function(error) {
                    console.error('Error checking device status:', error);
                }
            });
        }
    </script>
    @endpush


</x-backend.layouts.master>
