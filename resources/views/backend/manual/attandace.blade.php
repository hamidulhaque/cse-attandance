<x-backend.layouts.master>
    @section('header_message', 'Manual entry for attendance! ')
    @push('custom_css')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <style>
            .modal-header {
                border-bottom: none !important;
            }

            .modal-footer {
                border-top: none !important;
            }
        </style>
    @endpush
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-sm-flex justify-content-between align-items-start">
                        <div>
                            <h4 class="card-title">Manual Entry (Attendance)</h4>
                            <p class="card-description mb-0">Give manual attendance entry of an Employee.</p>
                        </div>
                        <div>
                            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal"
                                data-target="#bulkModal">
                                Bulk update
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <form class="form-prevent" action="{{ route('manualAttandance.post') }}" method="POST">
                            @csrf
                            @method('post')
                            <div class="row manual-entry">
                                <div class="col-md-3 col-sm-6">
                                    <div class="form-group">
                                        <label>Employee ID</label>
                                        <input type="text" class="form-control  employee-autocomplete"
                                            placeholder="Ex. 218325" name="emp_code" value="{{ old('emp_code') }}">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input type="date" class="form-control" name="date"
                                            value="{{ old('date') }}">
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6">
                                    <div class="form-group">
                                        <label>In Time</label>
                                        <input type="time" class="form-control" name="in_time"
                                            value="{{ old('in_time') }}">
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6">
                                    <div class="form-group">
                                        <label>Out Time</label>
                                        <input type="time" class="form-control" name="out_time"
                                            value="{{ old('out_time') }}">
                                    </div>
                                </div>


                                <div class="col-md-2 col-sm-6 align-self-center">
                                    <button
                                        class="btn btn-primary btn-sm w-100 form-prevent-multiple-submit">Submit</button>
                                </div>




                            </div>

                        </form>
                    </div>


                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="bulkModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header m-0 pb-0">
                    <h5 class="modal-title" id="exampleModalLabel">Bulk Manual Update</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pr-0">
                    <div class="mb-3" role="alert">
                        <p>Upload a CSV file with the required format:</p>
                        <ul>
                            <li>Column 1: Employee ID</li>
                            <li>Column 2: Date</li>
                            <li>Column 3: In</li>
                            <li>Column 3: Out</li>
                        </ul>
                        <p>
                            <span class="badge badge-pill badge-info">Cautious: </span> All the data in CSV file must
                            contain text type value.
                        </p>
                        <p>You can download a sample CSV file <a target="_blank"
                                href="{{ route('manual.sample.csv') }}">here</a>.</p>
                    </div>
                    <form action="" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="csvFile">Choose a CSV file to upload:</label>
                            <input class="form-control form-control-sm" id="csvFile" type="file" name="csvFile"
                                required>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
                </form>
            </div>
        </div>
    </div>


    @push('custom_js')
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <script src="{{ asset('backend/js/jquery-ui.js') }}"></script>
        <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
        <script>
            var empDropdownListRoute = "{{ route('emp.DropList') }}";
        </script>
        <script src="{{ asset('backend/js/manual-attandance.js') }}"></script>
    @endpush







</x-backend.layouts.master>
