<x-guest.layouts.master>
    <h4>Hello! let's get started</h4>
    <h6 class="fw-light">Sign in to continue.</h6>
    <form class="pt-3" action="" method="POST">
        @csrf
        @method('post')
        <div class="form-group">
            <input type="email" class="form-control form-control-lg"  id="exampleInputEmail1" placeholder="Username">
        </div>
        <div class="form-group">
            <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password">
        </div>
        <div class="mt-3">
            <a class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
                href="{{ asset('backend/index.html') }}">SIGN IN</a>
        </div>
        <div class="my-2 d-flex justify-content-between align-items-center">
            <div class="form-check">
                <label class="form-check-label text-muted">
                    <input type="checkbox" class="form-check-input">
                    Keep me signed in
                </label>
            </div>
            <a href="#" class="auth-link text-black">Forgot password?</a>
        </div>

    </form>
</x-guest.layouts.master>
