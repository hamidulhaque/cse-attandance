<x-guest.layouts.master>
    <h4>Enter your registered email to reset your password.</h4>
    <form class="pt-3">
        <div class="form-group">
            <input type="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email Addresss">
        </div>
        <h6 class="fw-light">You will get a email to your registered email address to recover your account.</h6>
        <div class="mt-3">
            <a class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
                href="{{ asset('backend/index.html') }}">Reset Password</a>
        </div>
        <div class="my-2 d-flex justify-content-between align-items-center">
            <a href="#" class="auth-link text-black">Sign in?</a>
        </div>
    </form>
</x-guest.layouts.master>
