<x-backend.layouts.master>

    


    <script>
        function greetAndSleep() {
            var currentTime = new Date().getHours();
            var greetingsElement = document.getElementById("greetings");

            if (currentTime < 12) {
                greetingsElement.textContent = "Good morning!";
            } else if (currentTime < 14) {
                greetingsElement.textContent = "Good noon!";
            } else if (currentTime < 18) {
                greetingsElement.textContent = "Good afternoon!";
            } else if (currentTime < 24) {
                greetingsElement.textContent = "Good evening! Have a relaxing night!";
            } else {
                greetingsElement.textContent = "Good night!";
            }
        }

        greetAndSleep();
    </script>
</x-backend.layouts.master>





























{{-- <x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{ __("You're logged in!") }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout> --}}
