<x-guest.layouts.master>
    <h4>Hello! let's get started</h4>
    <h6 class="fw-light">Sign in to continue.</h6>
    <x-auth-session-status class="mb-4" :status="session('status')" />
    <form class="pt-3" action="{{ route('login.check') }}" method="POST">
        @csrf
        @method('post')
        <div class="form-group">
            <input type="email" name="email" class="form-control form-control-lg" id="email" placeholder="Email"
                value="{{ old('email') }}">
            <x-input-error :messages="$errors->get('email')" class="mt-2" />
        </div>
        <div class="form-group">
            <input type="password" class="form-control form-control-lg" name="password" id="password"
                placeholder="Password" autocomplete="current-password">

            <x-input-error :messages="$errors->get('password')" class="mt-2" />
        </div>
        <div class="mt-3">
            <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">SIGN
                IN</button>
        </div>
        <div class="my-2 d-flex justify-content-between align-items-center">
            <div class="form-check">
                <label class="form-check-label text-muted">
                    <input type="checkbox" class="form-check-input" name="remember">
                    Keep me signed in
                </label>
            </div>
            <a href="{{ route('password.request') }}" class="auth-link text-black">Forgot password?</a>
        </div>

    </form>
</x-guest.layouts.master>
