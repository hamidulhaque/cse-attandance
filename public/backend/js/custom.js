function greetAndSleep() {
    var currentTime = new Date().getHours();
    var greetingsElement = document.getElementById("greetings");

    if (currentTime < 12) {
        greetingsElement.textContent = "Good morning!";
    } else if (currentTime < 14) {
        greetingsElement.textContent = "Good noon!";
    } else if (currentTime < 18) {
        greetingsElement.textContent = "Good afternoon!";
    } else if (currentTime < 24) {
        greetingsElement.textContent = "Good evening! Have a relaxing night!";
    } else {
        greetingsElement.textContent = "Good night!";
    }
}

greetAndSleep();


(function () {
    $('.form-prevent').on('submit', function () {
        $('.form-prevent-multiple-submit').attr('disabled', 'true');
        $('.form-prevent-multiple-submit').html('Processing...');
    });
})();

