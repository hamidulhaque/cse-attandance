$(document).ready(function () {
    initAutocomplete($(".approved"));

    function initAutocomplete(element) {
        element.autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: empDropdownListRoute,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        term: request.term
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        response(data);
                    },
                    error: function () {
                        console.error('AJAX request failed for autocomplete');
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                element.val(ui.item.value);
                return false;
            }
        });
    }
});
