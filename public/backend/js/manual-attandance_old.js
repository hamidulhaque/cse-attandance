$(document).ready(function () {

    function initAutocomplete(element) {
        element.autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: empDropdownListRoute,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        term: request.term
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        response(data);
                    },
                    error: function () {
                        console.error('AJAX request failed for autocomplete');
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                element.val(ui.item.value);
                return false;
            }
        });
    }
    function validateRow(row) {
        var formData = row.find('input').serializeArray();
    
        $.ajax({
            url: validateUrl,
            method: 'POST',
            dataType: 'json',
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                console.log('Validation response:', response);
    
                // Clear previous error messages
                row.find('.error-message').text('').hide();
    
                if (response.success) {
                    // Validation success
                    $('.form-prevent-multiple-submit').remove('disabled', true);
                    row.removeClass('has-error');
                } else {
                    // Validation error
                    row.find('.error-message').show();
                    row.find('.error-message').text(response.message);
    
                    // Disable submit button and add visual cue
                    $('.form-prevent-multiple-submit').prop('disabled', true);
                    $('.form-prevent-multiple-submit').addClass('disabled');
    
                    // Optionally, you can add a class or style to highlight the row with an error
                    row.addClass('has-error');
                }
    
                updateButtons();
            },
            error: function (xhr, status, error) {
                console.error('AJAX request failed:', status, error);
            }
        });
    }
    

    function updateButtons() {
        $('.manual-entry .addNew').hide();
        $('.manual-entry:last .addNew').show();

        var rowCount = $('.manual-entry').length;

        if (rowCount > 1) {
            $('.manual-entry .remove').show();
        } else {
            $('.manual-entry .remove').hide();
        }

        var hasErrors = $('.error-message:visible').length > 0;
        $('.form-prevent-multiple-submit').prop('disabled', hasErrors);
    }

    $(".employee-autocomplete").each(function () {
        initAutocomplete($(this));
        $(this).closest('.manual-entry').find('.error-message').hide();
    });

    $(document).on('change', '.manual-entry input', function () {
        var row = $(this).closest('.manual-entry');
        validateRow(row);
    });

    $(document).on('click', '.addNew', function () {
        var newRow = $('.manual-entry:last').clone();
        newRow.find('input').val('');
        initAutocomplete(newRow.find('.employee-autocomplete'));
        newRow.find('.error-message').text('').hide();
        $('.manual-entry:last').after(newRow);
        updateButtons();
        $('html, body').animate({
            scrollTop: newRow.offset().top + newRow.height()
        }, 500);
        validateRow(newRow);
    });

    $(document).on('click', '.remove', function () {
        var rowCount = $('.manual-entry').length;

        if (rowCount > 1) {
            $(this).closest('.manual-entry').remove();
        }

        updateButtons();
    });

    updateButtons();

});
