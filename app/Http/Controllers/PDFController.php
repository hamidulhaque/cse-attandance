<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Services\DayStatus\DayStatusService;
use App\Services\DayWise\DayWiseReport;
use App\Services\Leave\EmployeeLeave;
use Illuminate\Http\Request;
use PDF;
use App\Models\Trigger;

class PDFController extends Controller
{
    public function empIndividual(Request $request)
    {

        $empCode = $request->query('employee');
        $employee = Employee::where('emp_code', $empCode)->first();

        $startDate = $request->query('startDate');
        $endDate = $request->query('endDate');
        $queryDate = $request->query('queryDate');
        $employee = Employee::where('emp_code', $empCode)->first();

        $currentDate = \Carbon\Carbon::parse($startDate)->format('Y-m-d');
        $punchDataArray = [];

        $totalDelayDays = 0;
        $totalEarlyOutDays = 0;
        $totalDelayInMins = 0;
        $totalEarlyOutMins = 0;
        $totalWorkingMins = 0;
        $totalWeekends = 0;
        $totalHolidays = 0;
        $totalLeave = 0;

        $totalAbsent = 0;

        while ($currentDate <= \Carbon\Carbon::parse($endDate)->format('Y-m-d')) {
            $punchData = $this->getPunches($empCode, $currentDate);
            $statusService = new DayStatusService();
            $status = $statusService->getStatus($currentDate, $employee->department->dept_code,$empCode);
            if ($status) {
                if ($status == 'Holiday') {
                    $totalHolidays += 1;
                } elseif ($status == 'Weekend') {
                    $totalWeekends += 1;
                } elseif ($status == 'Leave') {
                    $totalLeave += 1;
                }
            }


            $date = date('d-m-y', strtotime($currentDate));
            $firstPunchTime = $punchData['first_punch'] ?? null;
            $lastPunchTime = $punchData['last_punch'] ?? null;

            if (!$firstPunchTime && !$lastPunchTime) {
                $firstPunchTime = null;
                $lastPunchTime = null;
                if($status == 0){
                    $status = 'Absent';
                    $totalAbsent += 1;
                }
            }


            // First punch delay
            if (isset($punchData['first_punch'])) {
                $scheduledTime = strtotime('09:00 AM');
                $actualTime = strtotime($punchData['first_punch']);


                $difference = ($scheduledTime - $actualTime) / 60;
                if ($difference >= 0) {
                    $formattedLateness = 'On time';
                } else {
                    $delayedMins = $difference * -1;
                    if ($delayedMins >= 60) {
                        $hours = floor($delayedMins / 60);
                        $minutes = $delayedMins % 60;
                        $formatted_duration = sprintf('%02d.%02d', $hours, $minutes) . ' Hrs';
                        $formattedLateness = $formatted_duration;
                    } else {
                        $formattedLateness = $delayedMins . ' Min';
                    }
                    $totalDelayDays += 1;

                    $totalDelayInMins += $delayedMins;
                }
            } else {
                $formattedLateness = '-';
            }


            // dd($formattedLateness);


            //lastpunch early-out
            if ($lastPunchTime) {
                $scheduledTime = strtotime('05:00 PM');
                $actualTime = strtotime($punchData['last_punch']);

                $difference = ($scheduledTime - $actualTime) / 60;

                if ($difference >= 0) {
                    if ($difference >= 60) {
                        $hours = floor($difference / 60);
                        $minutes = $difference % 60;
                        $formatted_duration = sprintf('%02d.%02d', $hours, $minutes) . ' Hrs';
                        $formatedEarlyMins = $formatted_duration;
                    } else {
                        $formatedEarlyMins = $difference . ' Min';
                    }
                    $totalEarlyOutDays += 1;
                    $totalEarlyOutMins += $difference;
                } else {
                    $formatedEarlyMins = 'On time';
                }
            } else {
                $formatedEarlyMins = '-';
            }



            //for calculating workingHour
            if ($firstPunchTime && $lastPunchTime) {
                $inTimeMins = strtotime($punchData['first_punch']);
                $outTimeMins = strtotime($punchData['last_punch']);

                $differenceInWH = ($outTimeMins - $inTimeMins) / 60; // Corrected calculation
                $totalWorkingMins += $differenceInWH;

                if ($differenceInWH >= 60) {
                    $hours = floor($differenceInWH / 60);
                    $minutes = $differenceInWH % 60;
                    $formatted_duration = sprintf('%02d.%02d', $hours, $minutes) . ' Hrs';
                    $workingHours = $formatted_duration;
                } else {
                    $workingHours = $differenceInWH . ' Min';
                }
            } else {
                $workingHours = '-';
            }


            $punchDataArray[$date] = [
                'day' => $date,
                'weekday' => \Carbon\Carbon::createFromFormat('d-m-y', $date)->format('l'),
                'first_punch' => $firstPunchTime,
                'delayTime' => $formattedLateness ?? '-',
                'last_punch' => $lastPunchTime,
                'earlyOut' => $formatedEarlyMins,
                'workingHour' => $workingHours,
                'status' => $status,
            ];

            $currentDate = date('Y-m-d', strtotime($currentDate . ' +1 day'));
        }

        // dd($totalWeekends);


        //delayed in min total
        if ($totalDelayInMins >= 60) {
            $hours = floor($totalDelayInMins / 60);
            $minutes = $totalDelayInMins % 60;
            $formatted_duration = sprintf('%02d.%02d', $hours, $minutes) . ' Hrs';
            $totalDelayed = $formatted_duration;
        } else {
            $totalDelayed = $totalDelayInMins . ' Min';
        }

        // working days
        $workDays = \Carbon\Carbon::parse($startDate)
            ->diffInDays(\Carbon\Carbon::parse($endDate)->addDay()) + 1; // Adding 1 to include the end date
        $workDays -= ($totalWeekends + $totalHolidays);


        //totalworking hours
        if ($totalWorkingMins >= 60) {
            $hours = floor($totalWorkingMins / 60);
            $minutes = $totalWorkingMins % 60;
            $formatted_duration = sprintf('%02d.%02d', $hours, $minutes) . ' Hrs';
            $totalWorkingHr = $formatted_duration;
        } else {
            $totalWorkingHr = $totalWorkingMins . ' Min';
        }


        //total-early-out
        if ($totalEarlyOutMins >= 60) {
            $hours = floor($totalEarlyOutMins / 60);
            $minutes = $totalEarlyOutMins % 60;
            $formatted_duration = sprintf('%02d.%02d', $hours, $minutes) . ' Hrs';
            $totalEarlyOutHrs = $formatted_duration;
        } else {
            $totalEarlyOutHrs = $totalEarlyOutMins . ' Min';
        }



        //due-hours
        $totalMins = ($workDays * 8) * 60;
        $difference = $totalMins - $totalWorkingMins;
        $hours = floor($difference / 60);
        $minutes = $difference % 60;
        $formatted_duration = sprintf('%02d.%02d', $hours, $minutes);
        $dueHours = $formatted_duration . ' Hrs';


        //percentage of working hours 
        $totalOfficialHours = $workDays * 8;
        $workingHoursPercentage = ($totalWorkingMins / ($totalOfficialHours * 60)) * 100;


        $workingPercentageCategory = '';

        if ($workingHoursPercentage <= 50) {
            $workingPercentageCategory = 'Poor';
        } elseif ($workingHoursPercentage <= 70) {
            $workingPercentageCategory = 'Average';
        } elseif ($workingHoursPercentage <= 84) {
            $workingPercentageCategory = 'Good';
        } elseif ($workingHoursPercentage <= 100) {
            $workingPercentageCategory = 'Excellent';
        } else {
            $workingPercentageCategory = 'Invalid Percentage'; // Handle this case based on your requirements
        }

        //ceil round

        $summary = [
            'workDays' => $workDays . ($workDays > 1 ? ' Days' : ' Day'),
            'remark' => $workingPercentageCategory,
            'percentage' => ceil($workingHoursPercentage) . ' %',
            'officialWorkingHr' => $workDays * 8 . ' Hrs',
            'totalDelayDays' => $totalDelayDays . ($totalDelayDays > 1 ? ' Days' : ' Day'),
            'totalDelayed' => $totalDelayed,
            'totalWorkingHr' => $totalWorkingHr,
            'totalEarlyOutDays' => $totalEarlyOutDays . ($totalEarlyOutDays > 1 ? ' Days' : ' Day'),
            'totalEarlyOutHrs' => $totalEarlyOutHrs,
            'totalLeave' => $totalLeave . ($totalLeave > 1 ? ' Days' : ' Day'),
            'totalAbsent' => $totalAbsent . ($totalAbsent > 1 ? ' Days' : ' Day'),
            'dueHours' => $dueHours,
            'weekends' => $totalWeekends . ($totalWeekends > 1 ? ' Days' : ' Day'),
            'holidays' => $totalHolidays . ($totalHolidays > 1 ? ' Days' : ' Day'),

        ];


        // dd($summary);


        $pdf = PDF::loadView('backend/pdfs/indivitualEmp', [
            'additionalData' => $punchDataArray,
            'employee' => $employee,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'queryDate' => $queryDate,
            'summary' => $summary
        ]);
        $name = 'attandance-report-' . $employee->emp_code . '-' . now()->format('d-m-y');
        return $pdf->stream($name . '.pdf');
    }

    public function getPunches($empCode, $date)
    {
        try {
            $punches = Trigger::where('emp_code', $empCode)
                ->whereDate('punch_time', $date)
                ->orderBy('punch_time')
                ->get();

            $firstPunchTime = null;
            $lastPunchTime = null;
            $isSunday = false;

            if (date('N', strtotime($date)) == 7) {
                $isSunday = true;
            }

            if ($punches->count() == 1) {
                $punchTime = $punches[0]->punch_time;

                if (strtotime($punchTime) < strtotime("$date 14:00:00")) {
                    $firstPunchTime = date('h:i A', strtotime($punchTime));
                } else {
                    $lastPunchTime = date('h:i A', strtotime($punchTime));
                }
            } else {
                $firstPunchTime = $punches->first() ? date('h:i A', strtotime($punches->first()->punch_time)) : null;
                $lastPunchTime = $punches->last() ? date('h:i A', strtotime($punches->last()->punch_time)) : null;
            }

            $result = [

                'first_punch' => $firstPunchTime,
                'last_punch' => $lastPunchTime,
                'holiday' => $isSunday,

            ];

            return $result;

        } catch (\Exception $e) {
            return [
                'error' => $e->getMessage(),
            ];
        }
    }



    public function dayWiseReport(Request $request)
    {
        $deptNames = $request->query('deptNames');
        $fromDate = $request->query('start_date');
        $toDate = $request->query('end_date');
        $queryDate = $request->query('queryDate');
        //  dd($deptNames, $fromDate, $end_date);
        $result = (new DayWiseReport)->EachEmployee($deptNames, $fromDate, $toDate);
        // $result = (new DayWiseReport)->EachEmployee($queryDate, $fromDate, $toDate);
        // dd($result);

        $pdf = PDF::loadView('backend.pdfs.daywise.dayDept', [
            'data' => $result,
            'startDate' => $fromDate,
            'endDate' => $toDate,
            'now' => $queryDate,
        ]);
        $name = 'report-day-dept' . $queryDate . '-' . now()->format('d-m-y');
        return $pdf->stream($name . '.pdf');

    }

}
