<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Trigger;
use Illuminate\Http\Request;

class DaywiseController extends Controller
{
    public function dayWise()
    {

        $departments = Department::all();
        // dd($departments);
        return view("backend.reports.daywise", compact('departments'));

    }

    public function DayWiseReport(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'dept-name' => 'nullable|array',
                'dept-code.*' => 'nullable|exists:departments,dept_code',
                'from-date' => 'required|date',
                'to-date' => 'nullable|date|after_or_equal:from-date',
            ]);
    
            $deptNames = $validatedData['dept-name'] ?? null;
            $fromDate = $validatedData['from-date'];
            $toDate = $validatedData['to-date'] ?? $fromDate;
    
            $result = $this->generateResult($deptNames, $fromDate, $toDate);
            dd($result);
            // return redirect()->back()->with(['result' => $result, 'success' => 'Your success message']);
            // return back()->with('success', 'Report generated successfully.')->with(compact('result'));
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
    
    private function generateResult($deptNames, $fromDate, $toDate)
    {
        $punchDataArray = [];
    
        $currentDate = $fromDate;
    
        while ($currentDate <= $toDate) {
            $punchData = $this->getPunches($deptNames, $currentDate);
            $date = date('d-m-y', strtotime($currentDate));
            $punchDataArray[$date] = $punchData;
    
            $currentDate = date('Y-m-d', strtotime($currentDate . ' +1 day'));
        }
    
        $expectedResult = [];
    
        foreach ($punchDataArray as $date => $employees) {
            foreach ($employees as $employee) {
                $departmentCode = $employee['department'];
                $employeeDetails = [
                    'name' => $employee['emp_name'] ?? null,
                    'employee_code' => $employee['emp_code'],
                    'department_name' => $this->getDepartmentName($departmentCode),
                    'in_time' => $employee['first_punch'] ?? null,
                    'out_time' => $employee['last_punch'] ?? null,
                    'holiday' => $employee['holiday'],
                ];
    
                $expectedResult[$departmentCode][] = [$employee['emp_code'] => $employeeDetails];
            }
        }
    
        return $expectedResult;
    }
    
    private function getPunches($deptNames, $date)
    {
        try {
            if (empty($deptNames)) {
                // Fetch all punches for the specified date range
                $punches = Trigger::whereDate('punch_time', $date)
                    ->orderBy('punch_time')
                    ->get();
            } else {
                $punches = Trigger::whereIn('department', $deptNames)
                    ->whereDate('punch_time', $date)
                    ->orderBy('punch_time')
                    ->get();
            }
    
            $punchDataArray = [];
    
            foreach ($punches as $punch) {
                $punchDataArray[] = [
                    "emp_name" => $punch->emp_name,
                    "emp_code" => $punch->emp_code,
                    "emp_position" => $punch->emp_position,
                    "day" => $date,
                    "department" => $punch->department,
                    "first_punch" => $punch->first_punch,
                    "last_punch" => $punch->last_punch,
                    "holiday" => date('N', strtotime($date)) == 7,
                ];
            }
    
            return $punchDataArray;
        } catch (\Exception $e) {
            return [
                'error' => $e->getMessage(),
            ];
        }
    }
    
       private function getDepartmentName($deptCode)
    {
        $department = Department::where('dept_code', $deptCode)->first();
        return $department ? $department->dept_name : null;
    }
  

    /*



if found 
{
    'dept-code'{
        'employee'{
            'name',
            'employee-code,
            'department-name',
            'in-time',
            'out-time,
            'holiday
        }
    },

    'dept-code'{
        'employee'{
            'name',
            'employee-code,
            'department-name',
            'in-time',
            'out-time,
            'holiday
        }
    },

}


if not 'dept-code'{
        'employee'{
            'name',
            'employee-code,
            'department-name',
            'null',
            'null',
            'holiday
        }
    },'dept-code'{
        'employee'{
            'name',
            'employee-code,
            'department-name',
            'null',
            'null,
            'holiday
        }
    },
*/






}
