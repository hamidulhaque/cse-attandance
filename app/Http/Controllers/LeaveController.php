<?php

namespace App\Http\Controllers;

use App\Models\LeaveType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Leave;
use Illuminate\Validation\Rule;
use App\Models\Employee;


class LeaveController extends Controller
{
    public function index()
    {
        $leave_types = LeaveType::all();
        return view('backend.leave.index', compact('leave_types'));
    }


    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'emp_code' => [
                    'required',
                    'string',
                    'max:255',
                    Rule::exists(Employee::class, 'emp_code'),
                ],
                'dates' => [
                    'required',
                    'string',
                    'regex:/^\d{2}-\d{2}-\d{4}(?:,\d{2}-\d{2}-\d{4})*$/',
                ],

                'approved' => [
                    'required',
                    'string',
                    'max:255',
                    Rule::exists(Employee::class, 'emp_code'),
                ],
                'leave_type' => [
                    'required',
                    'exists:leave_types,id',
                ],

                'cause' => [
                    'required',
                    'string',
                    'max:20',
                ],
            ]);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }


            $dateArray = explode(',', $request->input('dates'));
            // dd($dateArray);


            foreach ($dateArray as $date) {
                Leave::create([
                    'emp_code' => $request->input('emp_code'),
                    'date' => \Carbon\Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d'),
                    'leave_type_id'=> $request->input('leave_type'),
                    'cause' => $request->input('cause'),
                    'approved_by' => $request->input('approved'),
                    'created_by' => Auth::user()->id,

                ]);
            }

            return redirect()->back()->with('success', 'Leave records created successfully.');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

}
