<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Helpers\BioSyncApi;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;



class DeviceController extends Controller
{

    public function create()
    {
        return view('backend.devices.create');
    }



    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required|string|max:255',
                'address' => 'required|string|max:255',
                'port' => 'required|string|max:255',
                'username' => 'required|string|max:255',
                'password' => 'required|string|max:255',
                
            ]);

            
            // Create a new device instance and fill it with the form data
            $device = new Device;
            $device->name = $request->input('name');
            $device->address = $request->input('address');
            $device->port = $request->input('port');
            $device->username = $request->input('username');
            $device->default = true;
            // Hash the password before saving it to the database
            $device->password = $request->input('password');
            
            // Save the device to the database
            $device->save();

            return redirect()->route('device.list')->with('success', 'Device added successfully');
        } catch (Exception $e) {
            return redirect()->back()->withErrors('error', 'Error adding device: ' . $e->getMessage());
        }
    }



    public function list()
    {
        $devices = Device::latest()->get();
        return view('backend.devices.list', compact('devices'));
    }


    public function test(){
        $bioSyncApi = new BioSyncApi();
       $employee = $bioSyncApi->fetchEmployeeList();
        dd($employee);
    }


    public function checkDeviceStatus($id)
    {
        try {
            $device = Device::findOrFail($id);
            $response = Http::timeout(2)->get("{$device->address}:{$device->port}");
    
            return response()->json(['status' => $response->successful() ? 'running' : 'not running']);
        } catch (\Exception $e) {
            // Log the exception for further investigation
            \Log::error('Error in checkDeviceStatus: ' . $e->getMessage());
    
            // Return a generic error response
            return response()->json(['status' => 'not running']);
            // return response()->json(['status' => 'error'], 500);
        }   
    }





}
