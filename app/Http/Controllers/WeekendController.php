<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Weekend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class WeekendController extends Controller
{
    public function index()
    {
        $weekdays = DB::table('weekdays')->get();
        $departments = Department::all();
        // dd($weekdays);
        return view('backend.weekend.index', compact('weekdays', 'departments'));
    }
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'department' => [
                    'required',
                    $request->input('department') === 'all' ? Rule::in(['all']) : Rule::exists('departments', 'dept_code'),
                ],
                'weekday' => 'required|exists:weekdays,id',
                'date' => 'nullable|date',
                'remark' => 'nullable|string|max:30',

            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }


            if ($request['department'] === "all") {
                Weekend::create([
                    'weekdays_id' => $request['weekday'],
                    'annouced_date' => $request['date'] ??  now()->format('Y-m-d'),
                    'remark' => $request['remark'] ,
                    'all' => true,
                    'created_by' => Auth::user()->id,
                ]);
            }else{
                Weekend::create([
                    'weekdays_id' => $request['weekday'],
                    'annouced_date' => $request['date'] ??  now()->format('Y-m-d'),
                    'remark' => $request['remark'] ,
                    'all' => false,
                    'department_id' => $request['department'],
                    'created_by' => Auth::user()->id,
                ]);    
            }
            return redirect()->back()->with('success', 'Weekend entry added successfully!');


        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
