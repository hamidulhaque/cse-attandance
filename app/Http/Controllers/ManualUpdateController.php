<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Trigger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ManualUpdateController extends Controller
{
    public function attandenceView()
    {
        $manual = Trigger::where('manual_entry', true)->get();
        // dd($manual);
        return view("backend.manual.attandace");
    }




    public function attandencePost(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'emp_code' => 'required|exists:employees,emp_code',
                'date' => 'required|date|before_or_equal:today',
                'in_time' => 'date_format:H:i|nullable|required_without:out_time',
                'out_time' => 'date_format:H:i|nullable|required_without:in_time',
            ]);

            if ($request->has('in_time') && $request->has('out_time') && $request->input('in_time') == $request->input('out_time')) {
                return redirect()->back()->withErrors('In time and Out time cannot be the same!')->withInput();
            }

            $customMessages = [
                'emp_code.required' => 'The employee ID field is required.',
                'emp_code.exists' => 'The selected employee ID is invalid.',
                'date.required' => 'The date field is required.',
                'date.date' => 'The date must be a valid date.',
                'date.before_or_equal' => 'The date must be before or equal to today.',
                'in_time.date_format' => 'The in time field must be in the format H:i.',
                'in_time.required_without' => 'The in time field is required when out time is not present.',
                'out_time.date_format' => 'The out time field must be in the format H:i.',
                'out_time.required_without' => 'The out time field is required when in time is not present.',
            ];

            $validator->setCustomMessages($customMessages);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $employee = Employee::where('emp_code', $request->input('emp_code'))->first();

            // Prepare punch times based on user input
            $inTime = $request->input('in_time');
            $outTime = $request->input('out_time');

            $trigger = new Trigger();
            $trigger->emp_code = $employee->emp_code;
            $trigger->department = $employee->department->dept_name;

            if ($inTime && $outTime) {
                // Save one entry for in time
                $trigger->punch_time = $request->input('date') . ' ' . $inTime;
                $trigger->upload_time = now();
                $trigger->manual_entry_by = Auth::user()->id;
                $trigger->manual_entry = true;
                $trigger->save();

                // Save another entry for out time
                $trigger = new Trigger();
                $trigger->emp_code = $employee->emp_code;
                $trigger->department = $employee->department->dept_name;
                $trigger->punch_time = $request->input('date') . ' ' . $outTime;
                $trigger->upload_time = now();
                $trigger->manual_entry_by = Auth::user()->id;
                $trigger->manual_entry = true;
                $trigger->save();

            } elseif ($inTime) {
                // Save entry for in time only
                $trigger->punch_time = $request->input('date') . ' ' . $inTime;
                $trigger->upload_time = now();
                $trigger->manual_entry_by = Auth::user()->id;
                $trigger->manual_entry = true;
                $trigger->save();
            } elseif ($outTime) {
                // Save entry for out time only
                $trigger->punch_time = $request->input('date') . ' ' . $outTime;
                $trigger->upload_time = now();
                $trigger->manual_entry_by = Auth::user()->id;
                $trigger->manual_entry = true;
                $trigger->save();
            }

            return redirect()->back()->with('success', 'Attendance saved successfully');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }



    
    public function downloadSampleCsv()
    {
        $csvContent = "employee_id,date,in,out\n218325,2022-01-01,08:00 AM,05:00 PM";

        return Response::make($csvContent, 200, [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=sample_manual_attendance.csv',
        ]);
    }






}
