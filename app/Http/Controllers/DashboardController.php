<?php

namespace App\Http\Controllers;

use App\Models\DeviceSync;
use App\Services\DayStatus\DayStatusService;
use Illuminate\Http\Request;

use App\Helpers\BioSyncApi;

class DashboardController extends Controller
{
   public function index()
   {
      return view("dashboard");
   }

   public function syncDevice()
   {
      $logs = DeviceSync::latest()->limit(10)->get();

      return view("backend.devices.sync", compact('logs'));
   }
   
   public function test()
   {
       try {
           $bioSyncApi = new BioSyncApi();
           $bioSyncApi->synchronize();
       } catch (\Exception $e) {
           \Log::error($e);
           dd($e->getMessage());
       }
   }

   public function test2()
   {
      try{  
        $status = new DayStatusService();
        dd($status->getStatus('2024-02-17',1,'180824'));
      }catch(\Exception $e){

      }
   }


  

}
