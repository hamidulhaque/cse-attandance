<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Holiday;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class HolidayController extends Controller
{
    public function index()
    {
        return view('backend.holiday.index');
    }
    public function create()
    {
        return view('backend.holiday.create');
    }


    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required|string|max:255',
                'fromDate' => 'required|date',
                'toDate' => 'nullable|date|after_or_equal:fromDate',
                'remark' => 'nullable|string',
            ]);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            // Holiday::create([
            //     'title' => $request->input('title'),
            //     'from_date' => $request->input('fromDate'),
            //     'to_date' => $request->input('toDate'),
            //     'remark' => $request->input('remark'),
            //     'created_by' => Auth::user()->id,

            // ]);
            $startDate = Carbon::parse($request['fromDate']);
            if ($request['toDate'] == null) {
                $request['toDate'] = $request['fromDate'];
            }
            ;
            // If toDate is provided, use it; otherwise, set endDate to startDate
            $endDate = $request['toDate'];



            $datesInRange = [];
            while ($startDate->lte($endDate)) {
                $datesInRange[] = $startDate->toDateString();
                $startDate->addDay();
            }

            foreach ($datesInRange as $date) {
                Holiday::create([
                    'title' => $request->input('title'),
                    'date' => $date,
                    'remark' => $request->input('remark'),
                    'created_by' => Auth::user()->id,
                ]);
            }
            return redirect()->back()->with('success', 'Holiday created successfully.');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    public function search(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'fromDate' => 'required|date',
                'toDate' => 'nullable|date|after_or_equal:fromDate',
            ]);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $fromDate = $request->input('fromDate');
            $toDate = $request->input('toDate');

            $holidays = Holiday::whereBetween('from_date', [$fromDate, $toDate])
                ->orWhereBetween('to_date', [$fromDate, $toDate])
                ->orderBy('from_date', 'asc')
                ->get();



            return view('backend.holiday.index', ['holidays' => $holidays]);

        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }






}
