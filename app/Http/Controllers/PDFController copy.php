<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Services\DayStatus\DayStatusService;
use App\Services\DayWise\DayWiseReport;
use Illuminate\Http\Request;
use PDF;
use App\Models\Trigger;

class PDFController extends Controller
{
    public function empIndividual(Request $request)
    {

        $empCode = $request->query('employee');
        $employee = Employee::where('emp_code', $empCode)->first();

        $startDate = $request->query('startDate');

        $endDate = $request->query('endDate');

        $queryDate = $request->query('queryDate');

        $employee = Employee::where('emp_code', $empCode)->first();


        $currentDate = \Carbon\Carbon::parse($startDate)->format('Y-m-d');
        $punchDataArray = [];


        while ($currentDate <= \Carbon\Carbon::parse($endDate)->format('Y-m-d')) {
            $punchData = $this->getPunches($empCode, $currentDate);
            $statusService = new DayStatusService();
            $status = $statusService->getStatus($currentDate,$employee->department->dept_code);

            $date = date('d-m-y', strtotime($currentDate));
            $firstPunchTime = isset($punchData['first_punch']) ? $punchData['first_punch'] : null;

            $lastPunchTime = isset($punchData['last_punch']) ? $punchData['last_punch'] : null;
            if (!$firstPunchTime && !$lastPunchTime) {
                $firstPunchTime = null;
                $lastPunchTime = null;
            }


            



            $punchDataArray[$date] = [
                'day' => $date,
                'weekday' => \Carbon\Carbon::createFromFormat('d-m-y', $date)->format('l'),
                'first_punch' => $firstPunchTime,
                'last_punch' => $lastPunchTime,
                'status' => $status,
            ];

            $currentDate = date('Y-m-d', strtotime($currentDate . ' +1 day'));
        }

        // dd($punchDataArray);

        $pdf = PDF::loadView('backend/pdfs/indivitualEmp', [
            'additionalData' => $punchDataArray,
            'employee' => $employee,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'queryDate' => $queryDate,
        ]);
        $name = 'attandance-report-' . $employee->emp_code . '-' . now()->format('d-m-y');
        return $pdf->stream($name . '.pdf');
    }

    public function getPunches($empCode, $date)
    {
        try {
            // Fetch all punches for the specified date range
            $punches = Trigger::where('emp_code', $empCode)
                ->whereDate('punch_time', $date)
                ->orderBy('punch_time')
                ->get();

            // Initialize first and last punch times to null
            $firstPunchTime = null;
            $lastPunchTime = null;
            $isSunday = false;

            // Check if the given date is a Sunday
            if (date('N', strtotime($date)) == 7) {
                $isSunday = true;
            }

            // Adjustments for handling one punch on one day
            if ($punches->count() == 1) {
                $punchTime = $punches[0]->punch_time;

                // If the punch time is before 2 pm, consider it as in time
                if (strtotime($punchTime) < strtotime("$date 14:00:00")) {
                    $firstPunchTime = date('h:i A', strtotime($punchTime));
                } else {
                    // If the punch time is after 2 pm, consider it as out time
                    $lastPunchTime = date('h:i A', strtotime($punchTime));
                }
            } else {
                // If there are multiple punches, set the first and last punch times accordingly
                $firstPunchTime = $punches->first() ? date('h:i A', strtotime($punches->first()->punch_time)) : null;
                $lastPunchTime = $punches->last() ? date('h:i A', strtotime($punches->last()->punch_time)) : null;
            }

            // Create the multi-dimensional array
            $result = [

                'first_punch' => $firstPunchTime,
                'last_punch' => $lastPunchTime,
                'holiday' => $isSunday,

            ];

            // You can return the result or any other relevant data as needed
            return $result;

        } catch (\Exception $e) {
            // Handle any exceptions here
            // Log or return an error response as needed
            return [
                'error' => $e->getMessage(),
            ];
        }
    }



    public function dayWiseReport(Request $request){
        $deptNames = $request->query('deptNames');
         $fromDate = $request->query('start_date');
         $toDate = $request->query('end_date');
         $queryDate = $request->query('queryDate');
        //  dd($deptNames, $fromDate, $end_date);
        $result = (new DayWiseReport)->EachEmployee($deptNames, $fromDate, $toDate);
        // $result = (new DayWiseReport)->EachEmployee($queryDate, $fromDate, $toDate);
        // dd($result);

        $pdf = PDF::loadView('backend.pdfs.daywise.dayDept', [
            'data' => $result,
            'startDate' => $fromDate,
            'endDate' => $toDate,
            'now' => $queryDate,
        ]);
        $name = 'report-day-dept' . $queryDate . '-' . now()->format('d-m-y');
        return $pdf->stream($name . '.pdf');
    
    }

}
