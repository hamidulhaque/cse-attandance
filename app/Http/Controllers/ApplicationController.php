<?php

namespace App\Http\Controllers;

use App\Models\LeaveType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use PDF;
class ApplicationController extends Controller
{
    public function leaveCreate()
    {
        $leave_types = LeaveType::all();
        return view('backend.application.leaveCreate', compact('leave_types'));
    }



    public function leaveOutsiderPdf(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'Oname' => 'required|string',
                'oEmpID' => 'required|regex:/^[0-9]{6}$/',
                'Odesg' => 'required|string',
                'Omoble' => [
                    'required',
                    'string',
                    'regex:/^01[1-9]\d{8}$/',
                ],
                'OleaveType' => 'required|exists:leave_types,id',
                'OfromDate' => 'required|date',
                'OtoDate' => 'nullable|required|date|after_or_equal:OfromDate',

                'oCAddress' => 'nullable|string',
                'reason' => 'required|string',
                'OmakeUpdate' => 'nullable|date',
                'OmakeUptime' => 'nullable|date_format:H:i',
                'oPname' => 'required|string',
                'OPDeg' => 'required|string',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $leave = LeaveType::findOrFail($request['OleaveType']);


            $pdfData = [
                'date'=> now()->format('d-m-y'),
                'name' => $request['Oname'],
                'emp_code' => $request['oEmpID'],
                'designation' => $request['Odesg'],
                'phone' => $request['Omoble'],
                'leave_type' => $leave->title,
                'from_date' => Carbon::createFromFormat('Y-m-d', $request['OfromDate'])->format('d-m-y'),
                'to_date' => Carbon::createFromFormat('Y-m-d', $request['OtoDate'])->format('d-m-y'),

                'address' => $request['oCAddress'],
                'reason' => $request['reason'],
                'makeup_date' => Carbon::createFromFormat('Y-m-d', $request['OmakeUpdate'])->format('d-m-y'),
                'makeup_time' => Carbon::createFromFormat('H:i', $request['OmakeUptime'])->format('h:i A'),

                'person_name' => $request['oPname'],
                'person_designation' => $request['OPDeg'],

            ];

            // dd($pdfData);
            $pdf = PDF::loadView('backend.pdfs.application.leave', [
                'data' => $pdfData,
            ]);
            $name = 'leave-app-'.$pdfData['name'].now();
            return $pdf->stream($name . '.pdf');



        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->back()->with('error', $e->getMessage());
        }
    }


    private function generatePdf($data)
    {
        

    }



}
