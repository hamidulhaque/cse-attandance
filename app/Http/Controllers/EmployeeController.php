<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function list()
    {
        $employees = Employee::all();
        return view('backend.employees.list', compact('employees'));
    }


    public function empDropDownList(Request $request)
    {
        $term = $request->input('term');
    
        $employees = Employee::where('emp_code', 'like', "%{$term}%")
            ->orWhere('first_name', 'like', "%{$term}%")
            ->orWhere('last_name', 'like', "%{$term}%")
            ->select('emp_code', 'first_name', 'last_name')
            ->get();
    
        $result = $employees->map(function ($employee) {
            return [
                'label' => $employee->first_name . ' ' . $employee->last_name . ' - ' . $employee->emp_code,
                'value' => $employee->emp_code,
            ];
        });
    
        return response()->json($result);
    }
    


}
