<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Services\DayWise\DayWiseReport;
use Illuminate\Http\Request;

class DaywiseController extends Controller
{
    public function dayWise()
    {

        $departments = Department::all();
        // dd($departments);
        return view("backend.reports.daywise", compact('departments'));

    }


    public function DayWiseReport(Request $request)
    {

        try {
            $validatedData = $request->validate([
                'dept-name' => 'nullable|array',
                'dept-code.*' => 'nullable|exists:departments,dept_code',
                'from-date' => 'required|date',
                'to-date' => 'nullable|date|after_or_equal:from-date',
            ]);

            $deptNames = $validatedData['dept-name'] ?? null;
            $fromDate = $validatedData['from-date'];
            $toDate = $validatedData['to-date'] ?? $fromDate;

            $result = (new DayWiseReport)->EachEmployee($deptNames, $fromDate, $toDate);
            $now = now()->format('Y-m-d h:i A');

            return redirect()->back()
            ->with([
                'success' => 'Data processed successfully.',
                'additionalData' => $result,
                'deptNames' => $deptNames,
                'duration' => [
                    'start_date' => date('d-m-Y', strtotime($fromDate)),
                    'end_date' =>  date('d-m-Y', strtotime($toDate)),
                ],
                'now' => $now,
            ]);
            // dd($result);
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }



































    /*



if found 
{
    'dept-code'{
        'employee'{
            'name',
            'employee-code,
            'department-name',
            'in-time',
            'out-time,
            'holiday
        }
    },

    'dept-code'{
        'employee'{
            'name',
            'employee-code,
            'department-name',
            'in-time',
            'out-time,
            'holiday
        }
    },

}


if not 'dept-code'{
        'employee'{
            'name',
            'employee-code,
            'department-name',
            'null',
            'null',
            'holiday
        }
    },'dept-code'{
        'employee'{
            'name',
            'employee-code,
            'department-name',
            'null',
            'null,
            'holiday
        }
    },
*/






}
