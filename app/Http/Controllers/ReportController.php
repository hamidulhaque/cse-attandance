<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Services\DayStatus\DayStatusService;
use Illuminate\Http\Request;
use App\Models\Trigger;
use Illuminate\Support\Facades\Validator;



class ReportController extends Controller
{
    public function individualReportView()
    {
        return view("backend.reports.indivituals");
    }

    public function individualReports(Request $request)
    {
        try {
            
            $validatedData = $request->validate([
                'emp-code' => 'required|string',
                'from-date' => 'required|date',
                'to-date' => 'nullable|date|after_or_equal:from-date',
            ]);

            $empCode = $validatedData['emp-code'];
            $fromDate = $validatedData['from-date'];
            

            $toDate = $request['to-date'] ?? $validatedData['from-date'];
            $employee = Employee::where('emp_code', $empCode)->first();


            $currentDate = $fromDate;
            $punchDataArray = [];

            while ($currentDate <= $toDate) {
                $punchData = $this->getPunches($empCode, $currentDate);

                $date = date('d-m-y', strtotime($currentDate));
                $statusService = new DayStatusService();
                $status = $statusService->getStatus($currentDate,$employee->department->dept_code);
            
                
                $firstPunchTime = isset($punchData['first_punch']) ? $punchData['first_punch'] : null;
                $lastPunchTime = isset($punchData['last_punch']) ? $punchData['last_punch'] : null;

                
                if (!$firstPunchTime && !$lastPunchTime) {
                    $firstPunchTime = null;
                    $lastPunchTime = null;
                }

                $punchDataArray[$date] = [
                    'day' => $date,
                    'first_punch' => $firstPunchTime,
                    'last_punch' => $lastPunchTime,
                    'status' => $status,
                ];

                $currentDate = date('Y-m-d', strtotime($currentDate . ' +1 day'));
            }
            $now = now()->format('Y-m-d h:i A');


            
            return redirect()->back()
                ->with([
                    'success' => 'Data processed successfully.',
                    'additionalData' => $punchDataArray,
                    'startDate' => date('d-m-Y', strtotime($fromDate)),
                    'endDate' => date('d-m-Y', strtotime($toDate)),
                    'now' => $now,
                ])
                ->with(compact('employee'));

        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->back()->withInput()->withErrors([$e->getMessage()]);
        }
    }


    public function getPunches($empCode, $date)
    {
        try {
            
            $punches = Trigger::where('emp_code', $empCode)
                ->whereDate('punch_time', $date)
                ->orderBy('punch_time')
                ->get();

                
            $firstPunchTime = null;
            $lastPunchTime = null;
           

            if ($punches->count() == 1) {
                $punchTime = $punches[0]->punch_time;
                if (strtotime($punchTime) < strtotime("$date 14:00:00")) {
                    $firstPunchTime = date('h:i A', strtotime($punchTime));
                } else {
                    $lastPunchTime = date('h:i A', strtotime($punchTime));
                }
            } else {
                $firstPunchTime = $punches->first() ? date('h:i A', strtotime($punches->first()->punch_time)) : null;
                $lastPunchTime = $punches->last() ? date('h:i A', strtotime($punches->last()->punch_time)) : null;
            }

            $result = [

                'first_punch' => $firstPunchTime,
                'last_punch' => $lastPunchTime,

            ];

            return $result;

        } catch (\Exception $e) {
            return [
                'error' => $e->getMessage(),
            ];
        }
    }








}
