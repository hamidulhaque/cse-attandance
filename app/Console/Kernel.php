<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Run the command at 10:30 AM
        $schedule->call(function () {
            \Log::info('Scheduled task is running at 10:30 AM');
            \Artisan::call('sync:biotime-data');
        })->dailyAt('10:30');

        // Run the command at 2:00 PM
        $schedule->call(function () {
            \Log::info('Scheduled task is running at 2:00 PM');
            \Artisan::call('sync:biotime-data');
        })->dailyAt('14:00');

        // Run the command at 5:30 PM
        $schedule->call(function () {
            \Log::info('Scheduled task is running at 5:30 PM');
            \Artisan::call('sync:biotime-data');
        })->dailyAt('17:30');

        // Run the command at 4:30 PM with logging
        $schedule->call(function () {
            \Log::info('Scheduled task is running at 4:30 PM');
            \Artisan::call('sync:biotime-data');
        })->dailyAt('16:30');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
