<?php

namespace App\Console\Commands;

use App\Models\DeviceSync;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Helpers\BioSyncApi;
use Illuminate\Support\Facades\Log;


class SyncBioTimeData extends Command
{
    protected $signature = 'sync:biotime-data';

    protected $description = 'Synchronize BioTime data';





    // public function handle()
    // {
    //     try {
    //         $bioSyncApi = new BioSyncApi();
    //         $results = $bioSyncApi->synchronize();
    //         if (!$results['success']) {
    //             $errorMessage = 'BioSyncApi synchronization failed. ' . $results['message'];
    //             $this->logAndStoreDeviceSync($errorMessage, false);
    //             return Command::FAILURE;
    //         }
    //         return Command::SUCCESS;
    //     } catch (\Exception $e) {
    //         $errorMessage = 'BioSyncApi synchronization failed. Error: ' . $e->getMessage();

    //         $this->logAndStoreDeviceSync($errorMessage, false);
    //         Log::error($errorMessage);

    //         return Command::FAILURE;
    //     } finally {
    //         $now = Carbon::now();
    //         $message = 'BioSyncApi synchronization completed successfully at: ' . $now;
    //         $this->logAndStoreDeviceSync($message, true);

    //     }
    // }


    // private function logAndStoreDeviceSync($message, $status)
    // {
    //     Log::info($message);
    //     DeviceSync::create([
    //         'message' => $message,
    //         'status' => $status,
    //     ]);
    // }
    public function handle()
    {
        try {
            $results = (new BioSyncApi())->synchronize();

            if (!$results['success']) {
                $errorMessage = 'BioSyncApi synchronization failed. ' . $results['message'];
                $this->handleSyncFailure($errorMessage);
                return Command::FAILURE;
            }

            return Command::SUCCESS;
        } catch (\Exception $e) {
            $errorMessage = 'BioSyncApi synchronization failed. Error: ' . $e->getMessage();
            $this->handleSyncFailure($errorMessage);
            Log::error($errorMessage);

            return Command::FAILURE;
        } finally {
            $this->logAndStoreSyncCompletion();
        }
    }

    private function handleSyncFailure($errorMessage)
    {
        $this->logAndStoreDeviceSync($errorMessage, false);
    }

    private function logAndStoreSyncCompletion()
    {
        $now = Carbon::now();
        $message = 'BioSyncApi synchronization completed successfully at: ' . $now;
        $this->logAndStoreDeviceSync($message, true);
    }

    private function logAndStoreDeviceSync($message, $status)
    {
        Log::info($message);
        DeviceSync::create([
            'message' => $message,
            'status' => $status,
        ]);
    }




}
