<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'emp_code',
        'first_name',
        'last_name',
        'nickname',
        'hiredate',
        'gender',
        'emp_type',
        'mobile',
        'email',
        'area_code',
        'sync_time',
        'department_id',
        'position_id',
        'updated_by',
    ] ;


    public function department()
    {
        return $this->belongsTo(Department::class,'department_id','dept_code');
    }

    public function position(){
        return $this->belongsTo(EmployeePosition::class,'position_id','position_code');
    }
}
