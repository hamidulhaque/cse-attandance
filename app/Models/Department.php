<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    protected $fillable = [
        'dept_name',
        'dept_code',
        'sync_time',
    ];

      
    
        // public function employees()
        // {
            
        // }
    

        public function emp()
        {
            return $this->belongsToMany(Employee::class, 'departments', 'dept_code', 'id');
        }
        
        
}
