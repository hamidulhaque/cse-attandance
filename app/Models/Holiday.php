<?php

namespace App\Models;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    use HasFactory,Timestamp;

    protected $fillable = [
        "title",
        "date",
        "remark",
        'department_id',
        "created_by"
    ];
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }


}
