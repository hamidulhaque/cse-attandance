<?php

namespace App\Models;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Weekend extends Model
{
    use HasFactory, Timestamp;

    protected $fillable = [
        'title',
        'weekdays_id',
        'annouced_date',
        'remark',
        'all',
        'department_id',
        'created_by'
    ];

    public function weekday()
    {
        return $this->belongsTo(Weekday::class, 'weekdays_id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id','dept_code');
    }




}
