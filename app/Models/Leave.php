<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    use HasFactory;


    protected $fillable = [
        'leave_type_id',
        'emp_code',
        'approved_by',
        'date',
        'cause',
        'created_by',
    ];
}
