<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Trigger extends Model
{
    use HasFactory;

    protected $fillable = [
        'punch_id',
        'emp_code',
        'punch_time',
        'upload_time',
        'department',
        'terminal_sn',
        'sync_time',
        'manual_entry',
        'manual_entry_by'


    ];
    public function getFirstAndLastPunchBetweenDates($empCode, $startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate)->startOfDay();
        $endDate = Carbon::parse($endDate)->endOfDay();
        $punchData = [];
        $currentDate = $startDate->copy();
        while ($currentDate->lte($endDate)) {
            $currentDateString = $currentDate->toDateString();

            $firstPunch = $this->where('emp_code', $empCode)
                ->whereDate('punch_time', $currentDateString)
                ->orderBy('punch_time', 'asc')
                ->first();

            $lastPunch = $this->where('emp_code', $empCode)
                ->whereDate('punch_time', $currentDateString)
                ->orderBy('punch_time', 'desc')
                ->first();

            $punchData[$currentDateString] = [
                'intime' => optional($firstPunch)->punch_time,
                'outtime' => optional($lastPunch)->punch_time,
            ];

            $currentDate->addDay();
        }

        return $punchData;
    }

}
