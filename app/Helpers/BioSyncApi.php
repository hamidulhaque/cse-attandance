<?php

namespace App\Helpers;

use App\Models\Device;
use App\Models\DeviceSync;
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeePosition;
use App\Models\Trigger;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class BioSyncApi
{
    private $device;
    private $token;
    private $apiUrl;

    public function __construct()
    {
        $this->token = null;

        $device = Device::where('default', true)->first();
        if ($device && $this->isDeviceActive($device->address, $device->port)) {
            $this->device['port'] = $device->port;
            $this->device['address'] = $device->address;
            $this->device['username'] = $device->username;
            $this->device['password'] = $device->password;
            $this->api_connect();
        } else {

            $message = 'Device Sync failed: Not Active';
            $status = false;
            Log::error('No Device Found or Device is not active');
            activity()->log('No Device Found or Device is not active to sync');
            $this->deviceErrors($message, $status);

        }
    }


    private function deviceErrors($message, $status)
    {
        $sync = new DeviceSync();
        $sync['message'] = 'Device Sync failed: Not Active';
        $sync['status'] = false;
        $sync->save();
    }

    private function isDeviceActive($deviceAddress, $port, $timeout = 1)
    {

        $socket = @fsockopen($deviceAddress, $port, $errno, $errstr, $timeout);

        if ($socket === false) {
            activity()->log('Device is not active');
            return false;
        }

        fclose($socket);
        return true;
    }

    private function api_connect()
    {
        try {
            Log::info('Start api_connect');
            $bioTime_Username = $this->device['username'];
            $bioTime_Password = $this->device['password'];
            $bioTime_Port = $this->device['port'];
            $bioTime_Url = $this->device['address'];
            $apiUrl = 'http://' . $bioTime_Url . ':' . $bioTime_Port . '/';
            $this->apiUrl = $apiUrl;
            $authUrl = $apiUrl . 'jwt-api-token-auth/';
            $client = new Client();
            $formData = [
                'username' => $bioTime_Username,
                'password' => $bioTime_Password,
            ];

            $response = $client->post($authUrl, [
                'form_params' => $formData,
            ]);

            if ($response->getStatusCode() !== 200) {

                $this->deviceErrors('API request failed with status code: ' . $response->getStatusCode(), false);
                Log::error('API request failed with status code: ' . $response->getStatusCode());
                throw new \Exception('API request failed');

            }

            $tokenData = json_decode($response->getBody(), true);
            $this->token = 'JWT ' . $tokenData['token'];
            Log::info('End api_connect');
            activity()->log('Device Connected And Ready to sync');

        } catch (\Exception $e) {
            Log::error($e);
            $this->token = null;
            activity()->log('Something Went Wrong! While connecting to the api. More: ' . $e->getMessage());
            $this->deviceErrors($e->getMessage(), false);

            return [
                'success' => false,
                'message' => 'Something Went Wrong! While connecting to the api.' . $e->getMessage(),
            ];
        }
    }

    public function getToken()
    {
        return $this->token;
    }

    public function fetchDepartmentList()
    {
        try {
            $targetUrl = $this->apiUrl . 'personnel/api/departments/';
            $this->api_connect();
            $headers = [
                'Authorization' => $this->getToken(),
            ];

            $client = new Client(['headers' => $headers]);

            $page_size = 100; // Adjust the page size as needed
            $departments = $this->fetchDataPaginated($client, $targetUrl, $page_size);

            if (empty($departments)) {
                Log::error('No data found in department list.');
                activity()->log('No data found in department list.');
                return [
                    'success' => false,
                    'message' => 'No data found in department list.',
                ];
            }

            $time = now();
            foreach ($departments as $department) {
                $department_code = $department['dept_code'];

                Department::updateOrInsert(
                    ['dept_code' => $department_code],
                    [
                        'dept_name' => $department['dept_name'],
                        'sync_time' => $time,
                    ]
                );
            }

            Log::info("Department List synchronized at: $time");
            activity()->log('Department List synchronized.');
            $this->token = null;
            return [
                'success' => true,
                'message' => 'Department List synchronized',
            ];

        } catch (\Exception $e) {
            Log::error($e);
            activity()->log('Department List synchronization failed. More: ' . $e->getMessage());
            $this->token = null;
            $this->deviceErrors($e->getMessage(), false);
            return [
                'success' => false,
                'message' => $e,
            ];
        }
    }

    public function fetchEmployeePositions()
    {
        try {
            $targetUrl = $this->apiUrl . 'personnel/api/positions/';
            $this->api_connect();
            $headers = [
                'Authorization' => $this->getToken(),
            ];

            $client = new Client(['headers' => $headers]);

            $page_size = 100; // Adjust the page size as needed
            $positions = $this->fetchDataPaginated($client, $targetUrl, $page_size);

            if (empty($positions)) {
                Log::error('No data found in Employee Position list.');
                activity()->log('No data found in Employee Position list.');
                return [
                    'success' => false,
                    'message' => 'No data found in Employee Position list.',
                ];
            }

            $time = now();
            foreach ($positions as $position) {
                $position_code = $position['position_code'];

                EmployeePosition::updateOrInsert(
                    ['position_code' => $position_code],
                    [
                        'position_name' => $position['position_name'],
                        'sync_time' => $time,
                    ]
                );
            }

            Log::info("Employee Position data synchronized at: $time");
            activity()->log('Employee Position data synchronized at: ' . $time);
            $this->token = null;
            return [
                'success' => true,
                'message' => 'Employee Position data synchronized',
            ];

        } catch (\Exception $e) {
            Log::error($e);
            $this->deviceErrors($e->getMessage(), false);
            activity()->log('Employee Position Can not synchronized ' . $e->getMessage());
            $this->token = null;
            return [
                'success' => false,
                'message' => $e,
            ];
        }
    }

    public function fetchEmployeeList()
    {
        try {
            $targetUrl = $this->apiUrl . 'personnel/api/employees/';
            $this->api_connect();
            $headers = [
                'Authorization' => $this->getToken(),
            ];

            $client = new Client(['headers' => $headers]);

            $page_size = 100; // Adjust the page size as needed
            $totalCount = $this->fetchTotalCount($client, $targetUrl);
            $totalPages = ceil($totalCount / $page_size);

            $time = now();
            for ($currentPage = 1; $currentPage <= $totalPages; $currentPage++) {
                $employees = $this->fetchDataPaginated($client, $targetUrl, $page_size, $currentPage);

                if (empty($employees)) {
                    continue;
                }

                $this->processEmployees($employees, $time);
            }

            $this->token = null;
            return [
                'success' => true,
                'message' => 'Employee data synchronized',
            ];

        } catch (\Exception $e) {
            Log::error($e);
            activity()->log('Employee data not synchronized.' . $e->getMessage());
            $this->token = null;
            $this->deviceErrors($e->getMessage(), false);
            return [
                'success' => false,
                'message' => $e,
            ];
        }
    }


    private function fetchTotalCount($client, $targetUrl)
    {
        $response = $client->get($targetUrl);

        if ($response->getStatusCode() !== 200) {
            Log::error('API request failed with status code: ' . $response->getStatusCode());
            activity()->log('API request failed with status code: ' . $response->getStatusCode());
            return 0;
        }

        return json_decode($response->getBody(), true)['count'] ?? 0;
    }
    private function processEmployees($employees, $time)
    {
        foreach ($employees as $employeeData) {
            $empCode = $employeeData['emp_code'];

            Employee::updateOrInsert(
                ['emp_code' => $empCode],
                [
                    'first_name' => $employeeData['first_name'],
                    'last_name' => $employeeData['last_name'] ?? null,
                    'nickname' => $employeeData['nickname'] ?? null,
                    'hire_date' => $employeeData['hire_date'],
                    'gender' => $employeeData['gender'],
                    'emp_type' => $employeeData['emp_type'],
                    'mobile' => $employeeData['mobile'] ?? null,
                    'email' => $employeeData['email'],
                    'department_id' => $employeeData['department']['dept_code'],
                    'position_id' => $employeeData['position']['position_code'],
                    'sync_time' => $time,
                ]
            );
        }


    }

    public function fetchTriggers()
    {
        try {
            $targetUrl = $this->apiUrl . 'iclock/api/transactions/';
            $this->api_connect();
            $headers = [
                'Authorization' => $this->getToken(),
            ];

            $client = new Client(['headers' => $headers]);
            $response = $client->get($targetUrl);

            if ($response->getStatusCode() !== 200) {
                Log::error('API request failed Fetching Triggers data with status code: ' . $response->getStatusCode());
                activity()->log('API request failed Fetching Triggers data with status code: ' . $response->getStatusCode());
                return [
                    'success' => false,
                    'message' => 'Failed to fetch Triggers data from BioTime API.',
                ];
            }

            $resultData = json_decode($response->getBody(), true);
            $totalCount = $resultData['count'];

            $page_size = 1000;
            $totalPages = ceil($totalCount / $page_size);
            $transactions = $this->fetchDataChunk($client, $targetUrl, 20, $page_size);
            // dd($transactions);
            for ($currentPage = 1; $currentPage <= $totalPages; $currentPage++) {
                $transactions = $this->fetchDataChunk($client, $targetUrl, $currentPage, $page_size);

                if (empty($transactions)) {
                    continue;
                }
                $this->processTriggers($transactions);
            }
            Log::info("Employee data synchronized at: " . now());
            activity()->log('Employee data synchronized.');
            $this->token = null;
            return [
                'success' => true,
                'message' => 'Triggers data synchronized',
            ];

        } catch (\Exception $e) {
            Log::error($e);
            activity()->log('Triggers data not synchronized.' . $e->getMessage());
            $this->deviceErrors($e->getMessage(), false);
            $this->token = null;
            return [
                'success' => false,
                'message' => 'Triggers data not synchronized.' . $e->getMessage(),
            ];
        }
    }


    private function fetchDataPaginated($client, $targetUrl, $pageSize, $currentPage = 1)
    {
        $response = $client->get($targetUrl, [
            'query' => [
                'page' => $currentPage,
                'page_size' => $pageSize,
            ],
        ]);

        if ($response->getStatusCode() !== 200) {
            Log::error('API request failed with status code: ' . $response->getStatusCode());
            activity()->log('API request failed with status code: ' . $response->getStatusCode());
            return [];
        }

        return json_decode($response->getBody(), true)['data'] ?? [];
    }


    private function fetchDataChunk($client, $targetUrl, $currentPage, $pageSize)
    {
        $response = $client->get($targetUrl, ['query' => ['page' => $currentPage, 'page_size' => $pageSize]]);

        if ($response->getStatusCode() !== 200) {
            Log::error('API request failed Fetching Triggers data with status code: ' . $response->getStatusCode());
            activity()->log('API request failed Fetching Triggers data with status code: ' . $response->getStatusCode());
            return [];
        }

        return json_decode($response->getBody(), true)['data'] ?? [];
    }

    private function processTriggers($transactions)
    {
        $time = now();

        foreach ($transactions as $transaction) {
            $punchId = $transaction['id'];

            Trigger::updateOrInsert(
                [
                    'punch_id' => $punchId,
                    'emp_code' => $transaction['emp_code'],
                    'punch_time' => $transaction['punch_time'],

                ],
                [

                    'emp_code' => $transaction['emp_code'],
                    'department' => $transaction['department'],
                    'punch_time' => $transaction['punch_time'],
                    'upload_time' => $transaction['upload_time'],
                    'terminal_sn' => $transaction['terminal_sn'] ?? null,
                    'sync_time' => $time,
                ]
            );

        }
    }


    public function synchronize()
    {
        try {
            DB::beginTransaction();

            $departmentsResult = $this->fetchDepartmentList();
            $employeesResult = $this->fetchEmployeeList();
            $positionsResult = $this->fetchEmployeePositions();
            $triggersResult = $this->fetchTriggers();

            $allSuccessful = $departmentsResult['success'] && $employeesResult['success'] &&
                $positionsResult['success'] && $triggersResult['success'];

            $message = $allSuccessful ? 'Device synchronized successfully.' : 'Some data synchronization failed at: Sync ' . __LINE__;
            $status = $allSuccessful;

            $this->deviceErrors($message, $status);


            Log::info('Synchronization Details:', [
                'fetchDepartmentList' => $departmentsResult,
                'fetchEmployeeList' => $employeesResult,
                'fetchEmployeePositions' => $positionsResult,
                'fetchTriggers' => $triggersResult,
            ]);

            DB::commit();

            return [
                'success' => $allSuccessful,
                'message' => $message,
            ];

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Synchronization Failed: ' . $e->getMessage());
            Log::error($e);

            activity()->log('Synchronization Failed.' . $e->getMessage());
            $this->deviceErrors('Device synchronization failed: ' . __LINE__, false);
            $this->token = null;

            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }
    }





}
