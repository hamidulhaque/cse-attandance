<?php
namespace App\Services\DayStatus;

use App\Models\Holiday;
use App\Models\Leave;
use App\Models\Weekend;
use Illuminate\Support\Carbon;

class DayStatusService
{

    public function getStatus($date, $dept = null, $empCode = null)
    {
        try {

            $isHoliday = $this->isHoliday($date);
            $isWeekend = $this->isWeekend($date, $dept);

            $onLeave = $empCode ? $this->onLeave($date, $empCode) : false;


            if ($onLeave && ($isHoliday || $isWeekend)) {
                return "Leave";
            } elseif ($isHoliday && $isWeekend) {
                return "Holiday";
            } elseif ($isHoliday) {
                return "Holiday";
            } elseif ($isWeekend) {
                return "Weekend";
            } elseif ($onLeave) {
                return "Leave";
            } else {
                return 0;
            }

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    private function isHoliday($date)
    {
        return Holiday::where('date', $date)->exists();
    }



    private function isWeekend($date, $dept)
    {
        $dayOfWeek = Carbon::parse($date)->dayOfWeek;

        $globalWeekend = Weekend::where('all', true)
            ->where('annouced_date', '<=', $date)
            ->orderBy('annouced_date', 'desc')
            ->first();

        if ($globalWeekend && $globalWeekend->weekday->day == $dayOfWeek) {
            return true;
        }

        if ($dept) {
            $deptWeekend = Weekend::where('department_id', $dept)
                ->where('annouced_date', '<=', $date)
                ->orderBy('annouced_date', 'desc')
                ->first();

            if ($deptWeekend && $deptWeekend->weekday->day == $dayOfWeek) {
                return true;
            }
        }
        return false;
    }








    private function onLeave($date, $empCode)
    {
        return Leave::where('emp_code', $empCode)
            ->whereDate('date', $date)
            ->exists();
    }




}