<?php

namespace App\Services\DayWise;

use App\Models\Department;
use App\Models\Employee;
use App\Models\Trigger;

class DayWiseReport
{
    public function EachEmployee($deptNames = null, $fromDate, $toDate = null)
    {
        try {
            if (!$toDate) {
                $toDate = $fromDate;
            }
    
            $data = [];
    
            $currentDate = $fromDate;
            while (strtotime($currentDate) <= strtotime($toDate)) {
                $formattedDate = date('d-m-Y', strtotime($currentDate));
    
                if (!$deptNames) {
                    $employees = Employee::all();
    
                    foreach ($employees as $emp) {
                        $employee = [
                            'code' => $emp->emp_code,
                            'name' => $emp->first_name . ' ' . $emp->last_name ?? '',
                            'post' => $emp->position->position_name,
                        ];
    
                        $data[$emp->department->dept_name][$formattedDate][$emp->emp_code] = $this->getPunches($employee, $currentDate, $currentDate);
                    }
                } else {
                    foreach ($deptNames as $dept) {
                        $deptmentCode = $this->getDepartmentCode($dept);
                        $emps = Employee::where('department_id', $deptmentCode)
                            ->select('emp_code', 'first_name', 'last_name', 'department_id', 'position_id')
                            ->get();
    
                        foreach ($emps as $emp) {
                            $employee = [
                                'code' => $emp->emp_code,
                                'name' => $emp->first_name . ' ' . $emp->last_name ?? '',
                                'post' => $emp->position->position_name,
                            ];
    
                            $data[$emp->department->dept_name][$formattedDate][$emp->emp_code] = $this->getPunches($employee, $currentDate, $currentDate);
                        }
                    }
                }
    
                $currentDate = date('Y-m-d', strtotime($currentDate . ' +1 day'));
            }
    
            return $data;
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }
    
    private function getPunches($emp, $date)
    {
        $empCode = $emp['code'];
        $punchData = $this->getPunchData($empCode, $date);
        $isSunday = date('N', strtotime($date)) == 7;
 
        return [
            'name' => $emp['name'],
            'position' => $emp['post'],
            'first_punch' => $punchData['first_punch'],
            'last_punch' => $punchData['last_punch'],
            'holiday' => $isSunday,
        ];
    }
    
    private function getPunchData($empCode, $date)
    {
        try {
            $date = date('Y-m-d ', strtotime($date));
            // dd($date);
            $punches = Trigger::where('emp_code', $empCode)
                ->whereDate('punch_time', $date)
                ->orderBy('punch_time')
                ->get();
    
            $isSunday = date('N', strtotime($date)) == 7;
    
            $firstPunchTime = null;
            $lastPunchTime = null;
    
            if ($punches->count() > 0) {
                $firstPunchTime = date('h:i A', strtotime($punches->first()->punch_time));
    
                if ($punches->count() > 1) {
                    $lastPunchTime = date('h:i A', strtotime($punches->last()->punch_time));
                }
            }
    
            return [
                'first_punch' => $firstPunchTime,
                'last_punch' => $lastPunchTime,
                'holiday' => $isSunday,
            ];
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }
    

    private function getDepartmentCode($deptName)
    {
        $department = Department::where('dept_name', $deptName)->first();
        return $department ? $department->dept_code : null;
    }
}
