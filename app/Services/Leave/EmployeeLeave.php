<?php

namespace App\Services\Leave;

use App\Models\Leave;

class EmployeeLeave
{
    public function getEmpLeave($empCode, $date)
    {
        return Leave::where('emp_code', $empCode)
            ->whereDate('date', $date)
            ->exists();
    }













}